//import {username, password} from "../../../fixtures/magentoUser.json"
import userAR from '../../fixtures/userB2B/galileoAR.json'
import userCL from '../../fixtures/userB2B/galileoCL.json'
import userMX from '../../fixtures/userB2B/zoomMX.json'
import userCO from '../../fixtures/userB2B/zoomCO.json'
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

describe('B2B - Catalogo', () => {
    let user ;
    if(Cypress.env('urlb2b_2').includes("distribuidoragalileo.com/arg") ){
        user = userAR
    } else  if(Cypress.env('urlb2b_2').includes("distribuidoragalileo.com/chl") ){
        user = userCL
    } else if(Cypress.env('urlb2b_2').includes("distribuidorazoom.com/mex") ){
        user = userMX
    } else if(Cypress.env('urlb2b_2').includes("distribuidorazoom.com/col") ){
        user = userCO
    }
    it('Validacion 1 - Logeo', () => {
        //cy.visit(url+'chl/')
        cy.visit(Cypress.env('urlb2b_2'));
        cy.get('[data-label="ó"] > .social-login-btn').click();
        cy.get('#email').type(user.email);
        cy.get('.field.password > #pass').type(user.pass);

        cy.get('.button-container > #send2 > span').click();
        cy.wait(4000)
    })
    it('Validacion 2 - Catalogo + scroll', () => {
        cy.get('.nav-1 ').click();
        cy.scrollTo('bottom', {duration: 5000})
        cy.get('.nav-2 ').click();
        cy.get(':nth-child(3) > a > .product-item-info').should('exist')
        cy.get('.nav-5 ').click();
        cy.scrollTo('bottom', {duration: 5000})
        //cy.get('#product-addtocart-button').click();
        //cy.wait(2000)
        //cy.get('.button-link > span').click();
        //cy.get('.message > div').contains('Añadido Opti-Free® Rewetting Drops X 10ML a su carrito de compras.')
    })
    it('Validacion 3 -ficha producto', () => {
        cy.get('.nav-5 ').click();

        if(Cypress.env('urlb2b_2').includes("distribuidorazoom.com/mex") ){
            cy.get('#search').type('41JJAC120');
            cy.get('.actions > .action > span').click();
            cy.get(':nth-child(1) > a > .product-item-info > .custom-container-inner > .button-add-custom').click();
            cy.get('.page-title > .base').contains('Acuvue Oasys con HydraClear Plus');
            cy.get('#description').contains('La humedad se conserva en toda la superficie de cada lente durante todo el día, por lo que tus ojos se mantienen frescos desde el momento en que los pones hasta el momento en que los sacas.' );
            //cy.get('.fotorama__active > img').should('have.attr', 'src', Cypress.env('urlb2b_2')+'media/catalog/product/cache/a732553309687c21b5a8bbba1dfcaa67/g/o/gotas-opti-free-rewetting-drops-10ml.png')
            cy.get('#tab-label-additional-title').click();
            cy.get('#product-attribute-specs-table > :nth-child(1)').should('exist')
            cy.get('#product-attribute-specs-table > :nth-child(1) > .data').contains('41JJAC120');
        }else if(Cypress.env('urlb2b_2').includes("distribuidorazoom.com/col") ){
            cy.get('#search').type('21BLUT101');
            cy.get('.actions > .action > span').click();
            cy.get(':nth-child(1) > a > .product-item-info > .custom-container-inner > .button-add-custom').click();
            cy.get('.page-title > .base').contains('ULTRA');
            cy.get('#description').contains('Los lentes de contacto ULTRA, de los pioneros ópticos Bausch & Lomb, son lentes de contacto de uso mensual diseñados para hacer frente a las' );
            //cy.get('.fotorama__active > img').should('have.attr', 'src', Cypress.env('urlb2b_2')+'media/catalog/product/cache/a732553309687c21b5a8bbba1dfcaa67/g/o/gotas-opti-free-rewetting-drops-10ml.png')
            cy.get('#tab-label-additional-title').click();
            cy.get('#product-attribute-specs-table > :nth-child(1)').should('exist')
            cy.get('#product-attribute-specs-table > :nth-child(1) > .data').contains('21BLUT101');
        }else if(Cypress.env('urlb2b_2').includes("distribuidoragalileo.com/chl") ){
            cy.get('#search').type('31ALAO123');
            cy.get('.actions > .action > span').click();
            cy.get(':nth-child(2) > a > .product-item-info > .custom-container-inner > .button-add-custom').click();
            cy.get('.page-title > .base').contains('AIR OPTIX Plus HydraGlyde Multifocal');
            cy.get('.value > div > p').contains(' son el nuevo y mejorado producto de Alcon. Sobre la base de los lentes de contacto Air Optix Aqua Multifocal,' );
            //cy.get('.fotorama__active > img').should('have.attr', 'src', Cypress.env('urlb2b_2')+'media/catalog/product/cache/a732553309687c21b5a8bbba1dfcaa67/g/o/gotas-opti-free-rewetting-drops-10ml.png')
            cy.get('#tab-label-additional-title').click();
            cy.get('#product-attribute-specs-table > :nth-child(1)').should('exist')
            cy.get('#product-attribute-specs-table > :nth-child(1) > .data').contains('31ALAO123');
        }else{
            cy.get('#search').type('53ALOF104');
            cy.get('.actions > .action > span').click();
            cy.get(':nth-child(1) > a > .product-item-info > .custom-container-inner > .button-add-custom').click();
            cy.get('.page-title > .base').contains('Opti-Free Puremoist 300ml');
            cy.get('.value > div > p').contains('Solución de desinfección Multi-Acción contiene los ingredientes POLYQUAD MR y ALDOX MR que destruyen' );
            //cy.get('.fotorama__active > img').should('have.attr', 'src', Cypress.env('urlb2b_2')+'media/catalog/product/cache/a732553309687c21b5a8bbba1dfcaa67/g/o/gotas-opti-free-rewetting-drops-10ml.png')
            cy.get('#tab-label-additional-title').click();
            cy.get('#product-attribute-specs-table > :nth-child(1)').should('exist')
            cy.get('#product-attribute-specs-table > :nth-child(1) > .data').contains('53ALOF104');
        }

    })
})