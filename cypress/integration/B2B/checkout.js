import {username, password} from "../../fixtures/magentoUser.json"
import user from '../../fixtures/argentinaUser.json'
const {first_name, last_name, email2, street, ext_number, city,
post_code, phone_number, id_number, id_type, website} = user

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

describe('B2B  Galileo - Checkout', () => {
    let url = 'https://mc-staging.distribuidoragalileo.com/'
    beforeEach(() => {
        cy.visit(Cypress.env('urlb2b_2'))
        cy.get('[data-label="ó"] > .social-login-btn').click();
        cy.get('#email').type('lentest.testerlentes+2@gmail.com');
        //Galileo
        cy.get('.field.password > #pass').type('Prueba2020*');
        //cy.get('.field.password > #pass').type('tPeF3rBQ');
        cy.get('.button-container > #send2 > span').click();
        cy.wait(4000)

    })
    /*it('Validacion 1 - Logeo - vaciar carrito', () => {
        cy.get('[data-block="minicart"] > .action').click();
        //if(cy.get('.item-actions').should('exist')){
        //    cy.get('.item-actions > .actions-toolbar > .action-delete').click();
        //}
    })*/
    it('Validacion 2 - Logeo - compra', () => {
        cy.get('.logo').click();
        cy.get('.nav-5 ').click();
        if(Cypress.env('urlb2b_2') == "https://mc-staging.distribuidoragalileo.com/chl/"){
            cy.get('#search').type('31ALAO123');
            cy.get('.actions > .action > span').click();
            cy.get(':nth-child(1) > a > .product-item-info > .custom-container-inner > .custom-container-inner-add > .product-image-container > .product-image-wrapper > .product-image-photo').click();
            cy.get('.left_options_wrapper > :nth-child(2) > .control > .select-styled').click();
            cy.get(':nth-child(2) > .control > .select-options > .container-list > [rel="65692"]').click();
            cy.get(':nth-child(3) > .control > .select-styled').click();
            cy.get(':nth-child(3) > .control > .select-options > .container-list > [rel="65875"]').click();
            cy.get('#product-addtocart-button').click();
            cy.wait(2000)
            cy.get('.message > div').contains('Añadido Lente de Diágnostico AIR OPTIX Plus HydraGlyde Multifocal a su carrito de compras.')
            cy.get('.main > .secondary').click();
            //cy.wait(3000)
            //cy.get('.body-content-to-mobile > .opc-block-summary > .items-in-cart > div.title').contains('INFORMACIÓN DE COMPRA');
            //cy.get('.body-content-to-mobile > .opc-block-summary > .items-in-cart > .content.minicart-items > .minicart-items-wrapper > .minicart-items > .item > :nth-child(1) > .product-item-details > .product-item-inner > .product > .product-item-name').contains('Acuvue Oasys con HydraClear Plus');

        }else{
            cy.get('#search').type('Opti-Free Replenish x 300ml');
            cy.get('.actions > .action ').click();
            cy.get(':nth-child(2) > a > .product-item-info > .custom-container-inner > .custom-container-inner-add > .details > .product').click();
            cy.get('#product-addtocart-button').click();
            cy.wait(2000)
            cy.get('.message > div').contains('Añadido Opti-Free Replenish x 300ml a su carrito de compras.')
            cy.get('.main > .secondary').click();
            cy.get('.body-content-to-mobile > .opc-block-summary > .items-in-cart > div.title > strong > .title').contains('Información de compra');
            cy.get('.body-content-to-mobile > .opc-block-summary > .items-in-cart > .content.minicart-items > .minicart-items-wrapper > .minicart-items > .item > :nth-child(1) > .product-item-details > .product-item-inner > .product > .product-item-name').contains('Opti-Free Replenish x 300ml');
            //cy.get('.body-content-to-mobile > .opc-block-summary > .data > tbody > .incl > .amount > strong > .price').contains('$14.335');
            cy.get('[data-bind="submit: navigateToNextStep"] > .actions-toolbar > div.primary > .button').click();
            //cy.get(':nth-child(1) > .fieldset > :nth-child(2) > dd > strong').contains('');
            //cy.get(':nth-child(1) > .fieldset > :nth-child(3) > dd > strong').contains('');
            cy.get('.source > div.secondary > .button').click();

            /*cy.get('#co-shipping-form')
            cy.get('#checkout-step-shipping > .content-address-form').find('div[name="shippingAddress.telephone"]').type("5436");
            cy.get('#checkout-step-shipping > .content-address-form').find('div[name="shippingAddress.region_id"]').find('select').select('Antofagasta');
            cy.wait(1000)
            cy.get('#checkout-step-shipping > .content-address-form').find('div[name="shippingAddress.city"]').find('select').select('Antofagasta');

            cy.get('#checkout-step-shipping > .content-address-form').find('div[name="shippingAddress.custom_attributes.address_type_custom"]').find('select').select('Casa');
            cy.get('#checkout-step-shipping > .content-address-form').find('div[name="shippingAddress.street.0"]').find('input').type("Cll Fake 123");
            cy.get('#checkout-step-shipping > .content-address-form').find('div[name="shippingAddress.custom_attributes.address_ext_number"]').find('input').type("Cll Fake 123");
            cy.get('#opc-continue-form').click();
            */

            //cy.get('.source > div.secondary > .button').click();

            cy.get('.payment-method-title').click();
            cy.get(':nth-child(5) > div.primary > .primary').click();
            cy.get('.base').contains('¡Transacción exitosa!');
            //let it = cy.get('.number-text-').val();
        }




    })
})