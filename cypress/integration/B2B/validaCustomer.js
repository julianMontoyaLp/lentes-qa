Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

import userAR from '../../fixtures/companiesB2B/galileoAR.json'
import userCL from '../../fixtures/companiesB2B/galileoCL.json'
import userMX from '../../fixtures/companiesB2B/zoomMX.json'
import userCO from '../../fixtures/companiesB2B/zoomCO.json'

describe('B2B valida customers', () => {

    let user = ''

    if(Cypress.env('urlb2b_2') == "https://mc-staging.distribuidoragalileo.com/arg/"){
        user = userAR
    } else if(Cypress.env('urlb2b_2') == "https://mc-staging.distribuidoragalileo.com/chl/"){
        user = userCL
    } else if(Cypress.env('urlb2b_2') == "https://mc-staging.distribuidorazoom.com/mex/"){
        user = userMX
    } else if(Cypress.env('urlb2b_2') == "https://mc-staging.distribuidorazoom.com/col/"){
        user = userCO
    }
    beforeEach(() => {
        cy.visit('https://mc-staging.lentesplus.com/admin')
        cy.login('bruno.dionel', 'Lentesplus2021*')
    })

    it('Crear Company en B2B', () => {
        cy.get('#menu-magento-customer-customer > [onclick="return false;"]').click()
        cy.get('.item-company-index > a').click()
        cy.get('#add').click()
        cy.wait(2000)
       // cy.get('.change-status > .modal-inner-wrap > .modal-footer > .action-primary').click()
        cy.get('[data-index="company_name"] input[name="general[company_name]"]').type(user.companyName)
        cy.get('[data-index="company_email"] input[name="general[company_email]"]').type(user.companyEmail)
        cy.get('[data-index="information"] > .fieldset-wrapper-title > .admin__collapsible-title').click()
        cy.get('[data-index="id_type"] > .admin__field-control select[name="information[id_type]"]').select(user.idType)
        cy.get('[data-index="id_number"] input[name="information[id_number]"]').type(user.idNumber)

        cy.get('[data-index="address"] > .fieldset-wrapper-title > .admin__collapsible-title').click()
        cy.get('fieldset.admin__field input[name="address[street][0]"]').type(user.address)
        cy.get('[data-index="city"] input[name="address[city]"]').type(user.city)
        cy.get('[data-index="country_id"] select[name="address[country_id]"]').select(user.countryId)
        cy.get('[data-index="region_id"] select[name="address[region_id]"]').select(user.state_province)
        cy.get('[data-index="postcode"] input[name="address[postcode]"]').type(user.addressPostCode)
        cy.get('[data-index="telephone"] input[name="address[telephone]"]').type(user.telephone)
        cy.get('[data-index="company_admin"] > .fieldset-wrapper-title > .admin__collapsible-title').click()
        cy.get('[data-index="website_id"] select[name="company_admin[website_id]"]').select(user.websiteId)
        cy.get('[data-index="email"] input[name="company_admin[email]"]').type(user.companyAdmin.email)
        cy.get('[data-index="firstname"] input[name="company_admin[firstname]"]').type(user.companyAdmin.firstName)
        cy.get('[data-index="lastname"] input[name="company_admin[lastname]"]').type(user.companyAdmin.lastname)
        cy.get('[data-index="company_admin"] > .admin__fieldset-wrapper-content > .admin__fieldset > [data-index="id_type"] select[name="company_admin[id_type]"]').select(user.companyAdmin.idtype)
        cy.get('[data-index="company_admin"] > .admin__fieldset-wrapper-content > .admin__fieldset > [data-index="id_number"] input[name="company_admin[id_number]"]').type(user.companyAdmin.idNumber)

        cy.get('[data-index="settings"] > .fieldset-wrapper-title > .admin__collapsible-title').click()

        //los siguientes 3 se omiten en pruebas 4 nov, aclarar funcion
        //cy.get('.admin__action-multiselect').click()
        //cy.get('li.admin__action-group-option span').contains('B2B - Clientes Nuevos').click()
        //cy.get('.confirm > .modal-inner-wrap > .modal-footer > .action-primary').click()

        cy.get('#save-button').click()

        cy.get('.messages > .message')
            .should('have.class', 'success')
        cy.get('.message > div[data-ui-id="messages-message-success"]')
            .should('contain', user.successCreateMessage)

    })

    it('Validar Company Admin', () => {
        cy.get('#menu-magento-customer-customer > [onclick="return false;"]').click()
        cy.get('.item-customer-manage > a').click()
        cy.wait(15000)
        //cy.get('[data-bind="afterRender: $data.setToolbarNode"] > :nth-child(1) > .data-grid-filters-actions-wrap > .data-grid-filters-action-wrap > .action-default').click()
        cy.get('.admin__fieldset > :nth-child(10) input[name="email"]').clear().type(user.companyAdmin.email)
        cy.get('.admin__footer-main-actions > .action-secondary').click()
        cy.get('.data-row > :nth-child(4)')
            .should('be.visible')
        cy.wait(5000)
        cy.get('[data-role="grid-wrapper"]').scrollTo('right', { duration: 2000 })
        cy.get('.data-grid-actions-cell > .action-menu-item').click()

        cy.get('.page-title')
            .should('contain', user.companyAdmin.firstName)

        cy.get('#tab_customer').click()
        cy.get('[data-index="website_id"] select[name="customer[website_id]"]').should('have.value', user.websiteCode)

    })

    it('Eliminar Company',() => {
        cy.get('#menu-magento-customer-customer > [onclick="return false;"]').click()
        cy.get('.item-company-index > a').click()
        cy.wait(5000)
        cy.get('.page-title')
            .should('contain', 'Companies')
        cy.get('#fulltext').clear().type('{enter}')
        cy.wait(5000)
        cy.get('.data-grid-filters-action-wrap > .action-default').click()
        cy.get('.admin__fieldset > :nth-child(8) input[name="company_email"]').clear().type(user.companyEmail)
        cy.get('.admin__footer-main-actions > .action-secondary').click()
        cy.get('.data-row > :nth-child(2) > .data-grid-cell-content')
            .should('exist')

        cy.get('.admin__data-grid-wrap').scrollTo('right', { duration: 2000 })
        cy.get('.data-grid-actions-cell > .action-menu-item').click()
        cy.get('.page-title')
            .should('contain', user.companyName)

        cy.get('#company-edit-delete-button').click()
        cy.get('.confirm > .modal-inner-wrap > .modal-footer > .action-primary').click()

        cy.wait(5000)
        cy.get('#menu-magento-customer-customer > ._active').click()
        cy.get('.item-customer-manage > a').click()
        cy.wait(15000)
        //cy.get('[data-bind="afterRender: $data.setToolbarNode"] > :nth-child(1) > .data-grid-search-control-wrap > #fulltext').clear().type('{enter}')
        //cy.get('[data-bind="afterRender: $data.setToolbarNode"] > :nth-child(1) > .data-grid-filters-actions-wrap > .data-grid-filters-action-wrap > .action-default').click()
        cy.get('.admin__fieldset > :nth-child(10) input[name="email"]').clear().type(user.companyAdmin.email)
        cy.get('.admin__footer-main-actions > .action-secondary').click()
        cy.get('.data-row > :nth-child(3) > .data-grid-cell-content')
            .should('exist')

        cy.get('.data-grid-checkbox-cell-inner').click()
        cy.get('.col-xs-2 > .action-select-wrap > .action-select').click()
        cy.get('.col-xs-2 > .action-select-wrap > .action-menu-items > .action-menu > :nth-child(1) > .action-menu-item').click()
        cy.get('.action-primary').click()

        cy.get('.messages > .message')
            .should('have.class', 'success')
        cy.get('.message > div[data-ui-id="messages-message-success"]')
            .should('contain', '1 record(s) were deleted')
    })
})