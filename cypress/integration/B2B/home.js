//import {username, password} from "../../../fixtures/magentoUser.json"

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

describe('B2B  Galileo - Home', () => {
    //Acceso Galileo home sin login
    let alt_logo = ''
    let hubspot_link = ''
    let titulo_beneficios = ''
    let banners = ''
    let banner_inferior = ''
    if(Cypress.env('urlb2b_2').includes("distribuidoragalileo.com/chl") ){
        alt_logo = "Distribuidora Galileo"
        hubspot_link = 'https://meetings.hubspot.com/comercial629/reunirse-con-arlecy-zapata-galileo-chile'
        titulo_beneficios = "Beneficios de comprar en Distribuidora Galileo"
        banners = '.owl-carousel-custom-52 > .owl-stage-outer > .owl-stage > .active > .banner-item > a > .banner-image > .owl-lazy'
        banner_inferior = '.owl-carousel-custom-55 > .owl-stage-outer > .owl-stage > .active > .banner-item > a > .banner-image > .owl-lazy'
    }else if(Cypress.env('urlb2b_2').includes("distribuidoragalileo.com/arg") ){
        alt_logo = "Distribuidora Galileo"
        hubspot_link = 'https://meetings.hubspot.com/comercial629/reunirse-con-arlecy-zapata-galileo-argentina-'
        titulo_beneficios = "Beneficios de comprar en Distribuidora Galileo"
        banners = '.owl-carousel-custom-52 > .owl-stage-outer > .owl-stage > .active > .banner-item > a > .banner-image > .owl-lazy'
        banner_inferior = '.owl-carousel-custom-55 > .owl-stage-outer > .owl-stage > .active > .banner-item > a > .banner-image > .owl-lazy'
    }else if(Cypress.env('urlb2b_2').includes("distribuidorazoom.com/col") ){
        alt_logo = "Distribuidora Zoom"
        hubspot_link = "https://meetings.hubspot.com/comercial629/calendario-zoom-colombia-"
        titulo_beneficios = "Beneficios de comprar en Distribuidora Zoom"
        banners = '.owl-carousel-custom-46 > .owl-stage-outer > .owl-stage > .active > .banner-item > a > .banner-image > .owl-lazy'
        banner_inferior = '.owl-carousel-custom-49 > .owl-stage-outer > .owl-stage > .active > .banner-item > a > .banner-image > .owl-lazy'
    }else{
        alt_logo = "Distribuidora Zoom"
        hubspot_link = "https://meetings.hubspot.com/comercial629"
        titulo_beneficios = "Beneficios de comprar en Distribuidora Zoom"
        banners = '.owl-carousel-custom-46 > .owl-stage-outer > .owl-stage > .active > .banner-item > a > .banner-image > .owl-lazy'
        banner_inferior = '.owl-carousel-custom-49 > .owl-stage-outer > .owl-stage > .active > .banner-item > a > .banner-image > .owl-lazy'
    }
    it('Validacion 1 - Logo', () => {
        cy.visit(Cypress.env('urlb2b_2'))
        cy.get('.logo > img').should('have.attr', 'alt', alt_logo)
        //cy.get('.logo > img').should('have.attr', 'src', 'https://mc-staging.distribuidoragalileo.com/static/version1627932923/frontend/Galileo/galileo/default/images/img-galileo-logo.png')
    })
    it('Validacion 2 - Barra de Busqueda', () => {
        cy.get('#search').should('be.visible')
    })
    it('Validacion 3 - Menu superior', () => {
        if(Cypress.env('urlb2b_2').includes("distribuidoragalileo.com/chl") ){
            cy.get('.nav-1 > .level-top').should('be.visible').contains('Air Optix')
            cy.get('.nav-2 > .level-top').should('be.visible').contains('Dailies')
            cy.get('.nav-3 > .level-top').should('be.visible').contains('Cosméticos')
            cy.get('.nav-4 > .level-top').should('be.visible').contains('Optifree')
            cy.get('.nav-5 > .level-top').should('be.visible').contains('Catálogo')
            cy.get('.nav-6 > .level-top').should('be.visible').contains('Lentes de diagnóstico')
        }else if(Cypress.env('urlb2b_2').includes("distribuidoragalileo.com/arg") ){
            cy.get('.nav-1 > .level-top').should('be.visible').contains('Air Optix')
            cy.get('.nav-2 > .level-top').should('be.visible').contains('Dailies')
            cy.get('.nav-3 > .level-top').should('be.visible').contains('Cosméticos')
            cy.get('.nav-4 > .level-top').should('be.visible').contains('Optifree')
            cy.get('.nav-5 > .level-top').should('be.visible').contains('Catálogo')
            cy.get('.nav-6 > .level-top').should('be.visible').contains('Lentes de diagnóstico')
        }else if(Cypress.env('urlb2b_2').includes("distribuidorazoom.com/col") ){
            cy.get('.nav-1 > .level-top').should('be.visible').contains('Bausch + Lomb')
            cy.get('.nav-2 > .level-top').should('be.visible').contains('Soluciones y Gotas')
            cy.get('.nav-3 > .level-top').should('be.visible').contains('Catálogo')
        }else{
            cy.get('.nav-1 > .level-top').should('be.visible').contains('Bausch + Lomb')
            cy.get('.nav-2 > .level-top').should('be.visible').contains('Johnson & Johnson')
            cy.get('.nav-3 > .level-top').should('be.visible').contains('Alcon')
            cy.get('.nav-4 > .level-top').should('be.visible').contains('CooperVision')
            cy.get('.nav-5 > .level-top').should('be.visible').contains('Avizor')
            cy.get('.nav-6 > .level-top').should('be.visible').contains('Catálogo')
        }
    })
    it('Validacion 4 - Banners', () => {
        cy.get(banners).should('be.visible')
    })
    it('Validacion 5 - Banner inferior', () => {
        cy.get(banner_inferior).should('be.visible')

    })
    it('Validacion 6 - Seccion beneficios', () => {
        cy.get('.container-add > .inner-container').contains(titulo_beneficios)
        cy.get('.container-add > .inner-container').contains('Gestión de pedidos más')
        cy.get('.container-add > .inner-container').contains('Rapidez en tiempos de entrega')
        cy.get('.container-add > .inner-container').contains('Mejor seguimiento a las')
    })
    it('Validacion 7 - Productos de la semana', () => {
        if(Cypress.env('urlb2b_2') != "https://mc-staging.distribuidoragalileo.com/arg/" && Cypress.env('urlb2b_2') != "https://mc-staging.distribuidoragalileo.com/chl/" && Cypress.env('urlb2b_2') != "https://mc-staging.distribuidorazoom.com/col/" ){
            cy.get('.home-page-week-products').contains('Productos de la semana')
            cy.get('.owl-carousel-products-bestsell_products > .owl-stage-outer > .owl-stage > :nth-child(8)').should('exist')
        }
    })
    it('Validacion 8 - Footer', () => {
        cy.get('.footer-custom-section .container-items .heading').contains('Nuestros productos')
        cy.get('.footer-custom-section .container-items .heading').contains('Ayuda')
        cy.get('.footer-custom-section .container-items .heading').contains('Términos legales')
        cy.get('.footer-custom-section .container-items .heading').contains('¿Tiene dudas?')
    })
    it('Validacion 9- Visita Comercial', () => {
        cy.get('.appointment-button > .action').click();
        cy.get('#appointment-popup').should('exist')
        cy.get('.appointment-button-popup > a').should('have.attr', 'href', hubspot_link)
    })
    //Validacion de Logo, barra de busqueda, menus, banners, seccion beneficios, footer , link huspot “visita comercial”
})