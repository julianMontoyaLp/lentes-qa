//import {username, password} from "../../../fixtures/magentoUser.json"

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

describe('B2B  Galileo - Educacion', () => {
    //
    it('Validacion Educacion ', () => {
        //cy.visit(url+'chl/')
        let url = '';
        cy.visit(Cypress.env('urlb2b_2'))
        if(Cypress.env('urlb2b_2') == "https://mc-staging.distribuidorazoom.com/mex/"){
            url = 'https://mc-staging.distribuidorazoom.com/'
            cy.get(':nth-child(2) > .content > :nth-child(1) > a').click();
            cy.get('.topText > h3').contains('Información técnica sobre productos')

            //cy.get('.docsWrap_jj > :nth-child(2)').contains('ACUVUE')
            cy.get('.docsWrap_jj > :nth-child(1) > :nth-child(1) > a ').should('have.attr', 'href', 'https://distribuidorazoom.s3.us-east-2.amazonaws.com/jj/acuvue/calculadora_de_adaptacion_jj_vision_pro.mp4')
            cy.get('.docsWrap_jj > :nth-child(3) > .accorTtl > .docGroupTtl').click();
            cy.get('.active > .accorBody > :nth-child(1) > :nth-child(1) > a').should('have.attr', 'href', 'https://distribuidorazoom.s3.us-east-2.amazonaws.com/jj/acuvue/acuvue_vita/yo_recomiendo_acuvue_vita.pdf')
            cy.get(':nth-child(6) > .accorTtl > .docGroupTtl').click();
            cy.get('.active > .accorBody > .docGroup > :nth-child(1) > a').should('have.attr', 'href', 'https://distribuidorazoom.s3.us-east-2.amazonaws.com/jj/Cursos%20J_J/mejora_tu_practica_profesional_en_un_solo_lugar_JJ_VISION_PRO%20.mp4')
        }else if(Cypress.env('urlb2b_2') == "https://mc-staging.distribuidorazoom.com/col/"){
            // no existe educacion se prueba conoce la plataforma
            url = 'https://mc-staging.distribuidorazoom.com/'
            cy.get(':nth-child(2) > .content > :nth-child(1) > a').click();
            cy.get(':nth-child(2) > .buttonWrap > .btnItm > a ').contains('Conozca nuestra plataforma - Parte I')
            cy.get(':nth-child(2) > .buttonWrap > .btnItm > a ').should('have.attr', 'href', 'https://distribuidorazoom.s3.us-east-2.amazonaws.com/Conozca-nuestra+plataforma-ParteI-Distribuidora-ZOOM-Colombia.mp4')

            cy.get(':nth-child(3) > .buttonWrap > .btnItm > a ').contains('Conozca nuestra plataforma - Parte II')
            cy.get(':nth-child(3) > .buttonWrap > .btnItm > a ').should('have.attr', 'href', 'https://distribuidorazoom.s3.us-east-2.amazonaws.com/Conozca-nuestra+plataforma-ParteII-Distribuidora-ZOOM-Colombia.mp4')

            cy.get(':nth-child(5) > .buttonWrap > .btnItm > a ').contains('Cómo ingresar a su cuenta')
            cy.get(':nth-child(5) > .buttonWrap > .btnItm > a ').should('have.attr', 'href', 'https://distribuidorazoom.s3.us-east-2.amazonaws.com/Paso-a-paso-Distribuidora-ZOOM-Colombia.pdf')

        }else{
            url = 'https://mc-staging.distribuidoragalileo.com/'
            cy.get(':nth-child(2) > .content > :nth-child(1) > a').click();
            cy.get('.topText > h3').contains('Información técnica sobre productos Alcon')

            if(Cypress.env('urlb2b_2') == "https://mc-staging.distribuidoragalileo.com/chl/"){
                cy.get('.docsWrap > :nth-child(1)').contains('Air Optix')
                //cy.get(':nth-child(2) > :nth-child(2) > a ').should('have.attr', 'href', url+'media/pdf/AIR_OPTIX_plus_HydraGlyde.pdf')
                cy.get(':nth-child(5) > h1').contains('Dailies')
                cy.get(':nth-child(6) > .docItem > a').should('have.attr', 'href', url+'media/pdf/DAILIES.pdf')
                cy.get(':nth-child(14) > h1').contains('Familia Alcon')
                cy.get(':nth-child(15) > :nth-child(1) > a ').should('have.attr', 'href', 'https://drive.google.com/drive/u/0/folders/1QrWBzFm2k5wYGnfiQTyIz0TMjf3H7OV3')
            }else{
                cy.get('.docsWrap > :nth-child(1)').contains('Air Optix')
                cy.get(':nth-child(2) > :nth-child(1) > a ').should('have.attr', 'href', url+'media/pdf/migracion_HydraGlyde.pdf')
                cy.get(':nth-child(4) > h1').contains('Lentes cosméticos')
                cy.get(':nth-child(5) > :nth-child(1) > a').should('have.attr', 'href', url+'media/wysiwyg/landings/B2B/paleta_colores_freshlook.jpeg')
                cy.get(':nth-child(13) > h1').contains('Familia Alcon')
                cy.get(':nth-child(14) > :nth-child(1) > a ').should('have.attr', 'href', 'https://mc-staging.distribuidoragalileo.com/media/pdf/vademecum_alcon_2020_.pdf')
            }
        }


    })

})