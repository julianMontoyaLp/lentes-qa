//import {username, password} from "../../../fixtures/magentoUser.json"

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

describe('B2B  Galileo - Cotactenos', () => {

    it('Validacion 1 - Logo', () => {
        cy.visit(Cypress.env('urlb2b_2'))


        if(Cypress.env('urlb2b_2') == "https://mc-staging.distribuidorazoom.com/mex/"){
            cy.get(':nth-child(2) > .content > :nth-child(3) > a').click();
            cy.get('.itemSpace_phone > a').should('exist')
            cy.get('.itemSpace_phone > a > :nth-child(1) > .itemText').contains('+52 81 53507424')
            cy.get('.itemSpace_mail > a').should('exist')
            cy.get('.itemSpace_mail > a > :nth-child(1) > .itemText').contains('servicioalcliente@distribuidorazoom.com')
            cy.get('.itemSpace_chat > a').should('exist')
            //cy.get('#webWidget').click()
        }else if(Cypress.env('urlb2b_2') == "https://mc-staging.distribuidorazoom.com/col/"){
            cy.get(':nth-child(2) > .content > :nth-child(2) > a').click();
            cy.get('.itemSpace_phone > a').should('exist')
            cy.get('.itemSpace_phone > a > :nth-child(1) > .itemText').contains('+57 1 5412341')
            cy.get('.itemSpace_mail > a').should('exist')
            cy.get('.itemSpace_mail > a > :nth-child(1) > .itemText').contains('servicioalclienteco@distribuidorazoom.com')
            cy.get('.itemSpace_chat > a').should('exist')
        }
        else if(Cypress.env('urlb2b_2') == "https://mc-staging.distribuidoragalileo.com/arg/"){
            cy.get(':nth-child(2) > .content > :nth-child(3) > a').click();
            cy.get('.itemSpace_phone > a').should('exist')
            cy.get('.itemSpace_phone > a > :nth-child(1) > .itemText').contains(' +54 1151686159 ')
            cy.get('.itemSpace_mail > a').should('exist')
            cy.get('.itemSpace_mail > a > :nth-child(1) > .itemText').contains('servicioalclientear@distribuidoragalileo.com')
            cy.get('.itemSpace_chat > a').should('exist')
        }else{
            cy.get(':nth-child(2) > .content > :nth-child(3) > a').click();
            cy.get('.itemSpace_phone > a').should('exist')
            cy.get('.itemSpace_phone > a > :nth-child(1) > .itemText').contains('+56 2 32411931')
            cy.get('.itemSpace_mail > a').should('exist')
            cy.get('.itemSpace_mail > a > :nth-child(1) > .itemText').contains('servicioalcliente@distribuidoragalileo.com')
            cy.get('.itemSpace_chat > a').should('exist')
        }
        cy.get('.bubble').should('exist')
        //cy.get('.bubble').click();
        //cy.get('#webWidget').should('be.visible')
    })

})