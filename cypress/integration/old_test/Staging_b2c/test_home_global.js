/*
it('Validacion completa Home ', () => {
  cy.visit('https://mc-staging.lentesplus.com/co')
  //Valdiacion google meta
  cy.get('head').contains('GTM-MVC468J')

  //Validar existencia de estilos
  //cy.get('head').contains('styles-l.min.css')

  //Validar existencia de menu navegacion
  cy.get('nav').contains('Lentes de Contacto')
  cy.get('nav').contains('Soluciones')
  cy.get('nav').contains('Gotas hidratantes')
  cy.get('nav').contains('Cuidado y Belleza')
  cy.get('nav').contains('PlusUp')
  cy.get('nav').contains('Todos los productos')

  //Validar existencia de banner imagenes
  //cy.get('body').contains('owlcarouselslider/images/h/s/hs_pickit_mobile.png')

  //Validar existencia de Productos slides
  cy.contains('Pack Acuvue Oasys con HydraClear Plus')

  //Clic en el hub ¿Necesitas ayuda?
   cy.get('.panel .header').contains('Centro de Ayuda').click()

  //Clic en Ver catalogo
  cy.get('.navigation').contains('Todos los productos').click()
  cy.get('.products-grid').contains('SofLens 59')

  //Clic en re-comprar
  cy.get('.reorder-login').contains('Re-comprar').click()
  cy.get('.social-login').contains('Elige tu método de ingreso')
  //cy.get('.social-login').contains('Ya tengo cuenta Lentesplus').click()
  //cy.get('.social-login').get('#email').type("tesst")
  cy.get('.social-login').get('.mfp-close').click()

  //Clic en lentes de contacto
  cy.get('.navigation').contains('Lentes de Contacto').click()
  cy.get('.flex-ul').get('.level2').contains('Uso Diario').click()
  cy.get('.products-grid').contains('Dailies AquaComfort Plus x30')
  cy.get('.lpi-0102-logo').click()

  //Clic en soluciones
  cy.get('.navigation').contains('Soluciones').click()
  cy.get('.flex-ul .level2').contains('BioTrue')
  cy.get('.flex-ul .level2').contains('Renu')
  cy.get('.flex-ul .level2').contains('Opti Free')
  cy.get('#ui-id-39').click()
  cy.get('.products-grid').contains('Biotrue X 300ML')
  cy.get('.lpi-0102-logo').click()

  //Clic en Gotas Hidratantes
  cy.get('.navigation').contains('Gotas hidratantes').click()
  cy.get('.flex-ul .level2').contains('Renu')
  cy.get('.flex-ul .level2').contains('Freegen')
  cy.get('.flex-ul .level2').contains('Labsoft')
  cy.get('#ui-id-45').click()
  cy.get('.products-grid').contains('Renu Plus')
  cy.get('.lpi-0102-logo').click()


  //Clic en cuidado y belleza / lentes cosmeticos
  cy.get('.navigation').contains('Cuidado y Belleza').click()
  cy.get('.products-grid').contains('Macucaps')
  cy.get('.lpi-0102-logo').click()

  //Clic en ¿porque lentesplus?

  //10 - click en métodos de pago
  //11 - click en tic toc lentes

})
*/