/// <reference types="cypress" />
import {username, password} from "../../../fixtures/magentoUser.json"
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

describe('Crear producto desde el admin - en Staging - ', () => {

    beforeEach(() => {
        cy.visit('/')
        cy.login(username, password)
        // Catalogo
        cy.get('#menu-magento-catalog-catalog > [onclick="return false;"]').click()
        // All Products
        cy.get('.item-catalog-products > a').click()
    })

    it('New product', () => {

        cy.get('#add_new_product-button').contains('Add Product').click()

        cy.get('.page-title')
            .should('contain', 'New Product')

        cy.get('[data-index="sku"] input').type('SKU_test')
        cy.get('[data-index="status"] > .admin__field-control > .admin__actions-switch > .admin__actions-switch-label').click()
        cy.get('[data-index="name"] input').type('ProductName_test')
        cy.get('[data-index="container_price"] input').type('111')
        cy.get('[data-index="quantity_and_stock_status_qty"] input').type('1')
        //cy.get('#M5HNNH7 > .action-select').click()

                        //se Agrega descripción
        cy.get('[data-index="content"] > .fieldset-wrapper-title > .admin__collapsible-title').click();
        cy.get('#toggleproduct_form_short_description > :nth-child(1)').click();
        // cy.get('#toggleproduct_form_short_description').click()
        //cy.get('#product_form_short_description').type('Aquí una descripción del producto.')
        //cy.get('#editorproduct_form_short_description input').type('ProductName_test description')
        //cy.get('#product_form_short_description_ifr').type('ProductName_test description')

                //se Agregan imagenes
        cy.get('[data-index="gallery"] > .fieldset-wrapper-title > .admin__collapsible-title').click();
        //cy.get('#fileupload');

                //se agrega en tiendas
        cy.get('.admin__fieldset-product-websites > .fieldset-wrapper-title > .admin__collapsible-title').click();
        cy.get('[data-index="7"] > .admin__field-control > .admin__field > .admin__field-label').click();

                //Click en guardar
        cy.get('#save-button').click();
        //cy.get('[data-index="name"] input').type('ProductName_test')
        //cy.get('[data-index="name"] input').type('ProductName_test')

        cy.wait(2000);

        // Catalogo  verificacion en lista
        cy.get('#menu-magento-catalog-catalog > [onclick="return false;"]').click()
        // All Products buscar producto
        cy.get('.item-catalog-products > a').click()
        cy.get('[data-bind="afterRender: $data.setToolbarNode"] > :nth-child(1) > .data-grid-search-control-wrap > #fulltext').clear();
        cy.get('[data-bind="afterRender: $data.setToolbarNode"] > :nth-child(1) > .data-grid-search-control-wrap > #fulltext').type('ProductName_test');
        cy.get('[data-bind="afterRender: $data.setToolbarNode"] > :nth-child(1) > .data-grid-search-control-wrap > .action-submit').click();
        cy.get('[data-role="grid-wrapper"]').contains('td','ProductName_test');

            //Ingresar al detalle del producto
        cy.get('[data-role="grid-wrapper"]').contains('td','ProductName_test').click();


    })

})
