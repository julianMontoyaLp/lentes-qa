/// <reference types="cypress" />

import {username, password} from "../../../fixtures/magentoUser.json"
import user from '../../../fixtures/argentinaUser.json'
const {first_name, last_name, email2, street, ext_number, city,
post_code, phone_number, id_number, id_type, website} = user

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

describe('Crear customer desde el admin - en Staging - ', () => {

    beforeEach(() => {
        //cy.intercept('GET', '/rte/v1/configuration/**').as('config')
        //cy.intercept('GET', '/static/**/adminhtml/Lentesplus/backend/en_US/js-translation.json').as('adminHTML')
        //cy.intercept('/events/1/**&ref=https://mc-staging.lentesplus.com/admin/customer/index/index/key/**/').as('customer')
        //cy.intercept('GET','/admin/mui/index/render/key/**').as('req')

        cy.visit('/')
        cy.login(username, password)
        // Customers
        cy.get('#menu-magento-customer-customer > [onclick="return false;"]').click()
        // All Customers
        cy.get('.item-customer-manage > a').click()
        //   cy.wait(['@req', '@customer'], { requestTimeout:160000, responseTimeout: 160000 })
    })

    it('New Customer', () => {
       // cy.intercept('POST', '/admin/customer/index/validate/key/**/?isAjax=true').as('create_customer')

        cy.get('#add').contains('Add New Customer').click()

        cy.get('.page-title')
            .should('contain', 'New Customer')

        cy.scrollTo(0, 100, {duration: 2000})

        // Associate to website
        cy.get('[data-index="website_id"] select').select(website)

        cy.scrollTo(0, 400, {duration: 2500})

        // First Name
        cy.get('[data-index="firstname"] input').type(first_name)
        // Last Name
        cy.get('[data-index="lastname"] input').type(last_name)

        cy.scrollTo(0, 600, {duration: 2500})

        // Email
        cy.get('[data-index="email"] input').type(email2)
        // Numero de celular
        cy.get('[data-index="phone_number"] input').type(phone_number)
        // Identificatio Type
        cy.get('[data-index="id_type"] select').select(id_type)

        cy.scrollTo(0, 900, {duration: 2500})

        // Id Number
        cy.get('[data-index="id_number"] input').type(id_number)

        cy.scrollTo('top', {duration: 3000})

        cy.get('#save').click()

        //cy.wait('@create_customer', { requestTimeout:30000 })
        cy.wait(5000)

        cy.get('.message > div')
            .should('contain', 'You saved the customer.')

    })

    it('Customer view', () => {
        cy.searchCustomerByEmailAndWebsite(email2, website)

        cy.get('.page-title')
            .should('contain', last_name)

        cy.get('.admin__table-secondary > tbody > :nth-child(1) > td')
            .should('contain', 'Individual user')

        cy.get(':nth-child(9) > td')
            .should('contain', 'Argentina View')

        cy.get(':nth-child(10) > td')
            .should('contain', 'Lead no activo')

        cy.get('address')
            .should('contain', 'The customer does not have default billing address.')

        cy.scrollTo(0,300, {duration:2000})
        cy.wait(3000)
    })

    it('Addresses', () => {
        cy.wait(5000)
        cy.searchCustomerByEmailAndWebsite(email2, website)
        cy.get('#tab_address').click()
        cy.scrollTo(0,400, {duration:2000})

        // cy.get('.admin__data-grid-outer-wrap > .admin__data-grid-wrap > .data-grid > tbody > .data-grid-tr-no-data > td').as('listAddresses')
        //     .should('contain', "We couldn't find any records.")
        cy.wait(2000)
        cy.scrollTo(0,200, {duration:2000})
        cy.get('.add-new-address-button').click()

        cy.wait(50000)
        cy.get('#modal-title-14')
            .should('contain', 'Add/Update Address')

        cy.get('.customer_form_areas_address_address_customer_address_update_modal > .modal-inner-wrap').as('modalAddress')
        // Setea Default Billing Address
        cy.get('[data-index="default_billing"] > .admin__field-control > .admin__actions-switch > .admin__actions-switch-label').click()
        // Setea Default Shipping Address
        cy.get('[data-index="default_shipping"] > .admin__field-control > .admin__actions-switch > .admin__actions-switch-label').click()

        cy.get('@modalAddress').scrollTo(0,300, {duration:2000})
        cy.get('input[name="street[0]"]').clear().type(street)
        cy.get('select.admin__control-select[name="country_id"]').eq(1).select('AR')
        cy.wait(3000)
        cy.get('@modalAddress').scrollTo(0,600, {duration:2000})
        cy.get('select[name="region_id"]').select('Buenos Aires')
        cy.wait(5000)
        cy.get('.required[data-index="city_id"] select[name="city_id"]')
            .should('be.visible')
            .select('4375')
        // cy.get('equired[data-index="city_id"] select[name="city_id"]').select('Ferre')

        cy.get('@modalAddress').scrollTo(0,800, {duration:2000})
        cy.get('select[name="municipio"]').select('General Arenales')
        cy.get('[data-index="postcode"] input[type="text"][name="postcode"]').type('6027')
        // cy.get('input').eq(0).type('6027')
        cy.get('[data-index="telephone"] input[type="text"][name="telephone"]').type(phone_number)
        cy.get('input[type="text"][name="address_ext_number"]').eq(0).type(ext_number)
        cy.wait(1000)
        cy.get('@modalAddress').scrollTo('top', {duration:3000})
        cy.get('.page-actions > #save').click()
        cy.wait(10000)
        cy.scrollTo(0, 500, {duration: 4000})
        cy.get('.data-row > :nth-child(2) > .data-grid-cell-content')
            .should('exist')
    })

    it('Reward Points', () => {
        cy.searchCustomerByEmailAndWebsite(email2, website)
        cy.wait(2000)
        cy.scrollTo(0,800, {duration:4000})
        cy.wait(1000)
        cy.get('#tab_customer_edit_tab_reward_content').click()

        cy.get('[data-area-active="true"] > .admin__scope-old > .fieldset-wrapper > .fieldset-wrapper-title > .title')
            .should('contain', 'Reward Points Balance')

        cy.scrollTo(0,500, {duration:3000})
        cy.get('#reward_points_delta').type('300')
        cy.get('#save_and_continue').click()
        cy.wait(2000)

        cy.get('#messages > .messages > .message > div')
            .should('contain', 'You saved the customer.')
    })

    it('Borrar el Customer creado', () => {
        cy.searchCustomerByEmailAndWebsite(email2, website)
        cy.get('#customer-delete-button').click()
        cy.get('.action-primary').click()
        cy.wait(7000)
        cy.get('.message > div')
            .should('contain', 'You deleted the customer')
    })
})