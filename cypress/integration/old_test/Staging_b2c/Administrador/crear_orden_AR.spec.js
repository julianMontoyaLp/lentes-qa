/// <reference types="cypress" />

import {username, password} from "../../../fixtures/magentoUser.json"
import user from '../../../fixtures/argentinaUser.json'
const {first_name, last_name, email, street, ext_number, city,
post_code, phone_number, id_number, id_type, website} = user

import {extractUrl} from '../../../support/helpers'

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

describe('Generar Orden de Compra desde el admin en AR - en Staging - ', () => {
    cy.viewport('iphone-5')
    let url = ''
    let newCustomerCreated = true

    it('Generar Orden de Compra desde el admin en AR - Customer desde cero', () => {
        cy.intercept('POST', '/admin/sales/order_create/loadBlock/key/**/block/data?isAjax=true').as('order_create')
        cy.intercept('POST', '/admin/sales/order_create/loadBlock/key/**/block/header,data?isAjax=true').as('order_create2')
        cy.intercept('POST', '/admin/sales/order_create/loadBlock/key/**/block/card_validation?isAjax=true').as('card_validation')
        cy.intercept('POST', '/events/1/**').as('post1')
        cy.intercept('POST', '/admin/sales/order_create/loadBlock/key/**/block/shipping_method,totals?isAjax=true').as('shipping_method')
        cy.intercept('POST', '/admin/sales/order_create/loadBlock/key/**/block/shipping_method,totals,billing_method?isAjax=true').as('totals_billing_method')
        cy.intercept('POST', '/admin/sales/order_create/loadBlock/key/**/block/billing_method,totals?isAjax=true').as('billing_method')

        cy.visit('/')
        cy.login(username, password)

        // assert nome usuario
        cy.get('.admin-user-account-text')
            .should('contain', username)

        // asert Dashboard
        cy.get('.page-title')
            .should('contain', 'Dashboard')

        // Sales > Orders
        cy.get('#menu-magento-sales-sales > [onclick="return false;"]').click()
        cy.get('.item-sales-order > a').click()

        cy.wait(10000)
        // assert Order
        cy.get('.page-title')
            .should('contain', 'Orders')

        // Click botón Create New Order
        cy.get('#add').click()
        // Click botón Create New Customer
        cy.wait(5000)
        cy.get('#order-customer-selector > .admin__page-section-title button[title="Create New Customer"]').click()
        // Selecciono Store Chile
        cy.get(':nth-child(3) > .admin__field-control > .nested > .admin__field > .admin__field-label').click()

        cy.wait('@order_create2', { requestTimeout:160000, responseTimeout: 160000 })
        cy.wait(['@order_create', '@post1', '@card_validation'], { responseTimeout: 160000 })
        // Click botón Add Products
        cy.get('#add_products').click()
        cy.scrollTo(0, 400, {duration: 2000})
        // Selecciono producto
        cy.get('#sales_order_create_search_grid_filter_entity_id').type('868{enter}')
        cy.wait(8000)
        cy.get('#sales_order_create_search_grid_table > tbody tr').click()
        cy.scrollTo(0, 200, {duration: 2000})
        // Click botón Add Selected Product(s) to Order
        cy.get('#order-search > .admin__page-section-title button').click()

        // Comienzo a ingresar los datos del nuevo usuario
        cy.wait(7000)
        cy.scrollTo(0, 600, {duration: 2000})
        cy.get('#email').type(email)
        cy.get('#id_type').select(id_type)
        cy.get('#id_number').type(id_number)

        cy.scrollTo(0, 1100, {duration: 2000})
        cy.get('#order-billing_address_firstname').type(first_name)
        cy.scrollTo(0, 1500, {duration: 2000})
        cy.get('#order-billing_address_lastname').type(last_name)
        cy.get('#order-billing_address_street0').type(street)
        cy.scrollTo(0, 1900, {duration: 2000})
        cy.get('#order-billing_address_region_id').select(city)
        cy.get('#order-billing_address_city').type(city)
        cy.get('#order-billing_address_postcode').type(post_code)
        cy.get('#order-billing_address_telephone').type(phone_number)
        cy.scrollTo(0, 2500, {duration: 2000})
        cy.get('#order-billing_address_address_ext_number').type(ext_number)

        cy.scrollTo(0, 3100, {duration: 2000})
        // Seleccion metodo de pago
        cy.get('#order-billing_method_summary > .action-default > span').trigger('click')
        cy.wait('@billing_method')
        cy.get('.admin__payment-methods > :nth-child(3) > .admin__field-label').click()
        // Seleccion metodo de envio
        cy.get('#order-shipping-method-summary > .action-default > span').click()
        cy.wait('@shipping_method')
        cy.get(':nth-child(2) > .admin__order-shipment-methods-options-list > .admin__field-option > .admin__field-label').click()

        cy.wait('@totals_billing_method')
        cy.get('#submit_order_top_button').click()
        // Vuelvo a seleccionar metodo de pago
        cy.get('.admin__payment-methods .admin__field-label').contains('Lentesplus - Url de Pago').click()
        cy.wait('@card_validation')
        cy.get('#submit_order_top_button').click()

        cy.get('div[data-ui-id="messages-message-success"]')
            .should('contain', 'You created the order.')

        cy.get('div[data-ui-id="messages-message-success"]').then(message => {
            newCustomerCreated = true
        })

        cy.scrollTo(0, 300, {duration: 2000})
        cy.wait(1500)
        cy.get('table.order-information-table #order_status')
            .should('contain', 'Pago Pendiente')

        // Website
        cy.get('.order-information > .admin__page-section-item-content > .admin__table-secondary > tbody > :nth-child(4) > td')
            .should('contain', website)

        // Nombre usuario
        cy.get('.order-account-information > .admin__page-section-item-content > .admin__table-secondary > tbody > :nth-child(1) > td')
            .should('contain', last_name)

        // Email
        cy.get('.admin__table-secondary > tbody > :nth-child(2) > td > a')
            .should('contain', email)

        // Customer Group
        cy.get('.order-account-information > .admin__page-section-item-content > .admin__table-secondary > tbody > :nth-child(3) > td')
            .should('contain', 'Lead no activo')

        // Tipo identificacion
        cy.get('.order-account-information > .admin__page-section-item-content > .admin__table-secondary > tbody > :nth-child(4) > td')
            .should('contain', id_type)

        // Nro DNI
        cy.get('.order-account-information > .admin__page-section-item-content > .admin__table-secondary > tbody > :nth-child(5) > td')
            .should('contain', id_number)

        cy.scrollTo(0, 900, {duration: 2000})
        cy.wait(1500)
        // Metodo de Pago
        cy.get('.order-payment-method-title')
            .should('contain', 'Lentesplus - Url de Pago  ')

        cy.scrollTo(0, 1900, {duration: 2000})
        cy.wait(1500)
        // Ultimo estado historial
        cy.get('#order_history_block > .note-list > :nth-child(1) > .note-list-status')
            .should('contain', 'Pago Pendiente')

        // Precio Total
        cy.get('.col-3 > :nth-child(2) > strong > .price')
            .should('contain', '$250.00')

        // Generar Url de pago
        cy.get('.page-main-actions').contains('Generate URL Pay').click()

        cy.get('.messages > .message')
            .should('have.class', 'message-success')

        cy.get('.message > div')
            .should('contain', 'The url pay is in the next link')

        // Obtengo URL
        cy.get('.message > div').then(($message) => {

            url = extractUrl($message.text())
            cy.log(url)
        })
    })

    it('Voy hacia la url de pago', () => {

        if(url === '') {
            cy.log('Error al crear la orden, no se generó la url de pago')
            // Fuerzo un error en el test
            cy.get('.message')
                .should('contain', 'Fuerzo un error en el test')
        }

        // Voy hacia la URL de pago
        cy.visit(url)

        cy.url()
            .should('include', '/ar/checkout/')
        cy.get('.base')
            .should('contain', 'Finaliza tu compra')
    })

    it('Borrar el Customer creado', () => {
        if(newCustomerCreated === false) {
            cy.log('Error al crear la orden, no se generó el nuevo customer')
            // Fuerzo un error en el test
            cy.get('.message')
                .should('contain', 'Fuerzo un error en el test')
        }
        cy.intercept('GET', '/rte/v1/configuration/**').as('config')
        cy.intercept('GET', '/static/**/adminhtml/Lentesplus/backend/en_US/js-translation.json').as('adminHTML')
        cy.intercept('/events/1/**&ref=https://mc-staging.lentesplus.com/admin/customer/index/index/key/**/').as('customer')

        cy.visit('/')
        cy.login(username, password)
        // Customers > All Customers
        cy.get('#menu-magento-customer-customer > [onclick="return false;"]').click()
        cy.get('.item-customer-manage > a').click()
        cy.wait(['@config', '@adminHTML'])
        cy.wait(['@customer'], { requestTimeout:160000, responseTimeout: 160000 })
        cy.searchCustomerByEmailAndWebsite(email, website)
        cy.get('#customer-delete-button').click()
        cy.get('.action-primary').click()
        cy.wait(7000)
        cy.get('.message > div')
            .should('contain', 'You deleted the customer')
    })

})