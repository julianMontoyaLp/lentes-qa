import {username, password} from "../../../fixtures/magentoUser.json"
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

describe('Creación gift card - Staging', () => {
    beforeEach(() => {
        cy.visit('https://mc-staging.lentesplus.com/admin')
        cy.login(username, password)

        // Marketing -> Gift Card Accounts
        cy.get('#menu-magento-backend-marketing > [onclick="return false;"]').click()
        cy.get('.item-customer-giftcardaccount > a > span').click()
        cy.get('.page-title')
            .should('contain', 'Gift Card Accounts')
    })
    it('Creación gift card - Colombia', () => {

        //////Gift Colombia
        // Añadir gift nueva
        cy.get('#add > span').click()

        //Information - Completar campos de la gift
        cy.contains('Active').scrollIntoView()
        cy.get('#_infostatus')
            .should('contain', 'Yes')
        cy.get('#_infois_redeemable')
            .should('contain', 'Yes')
        cy.get('#_infowebsite_id').select('Colombia Website')
        cy.get('#_infobalance').type('100000')
        cy.wait(1000)

        //Send gift card
        cy.get('[data-ui-id="adminhtml-giftcardaccount-edit-tabs-0-tab-item-send"] > #giftcardaccount_info_tabs_send > :nth-child(1)').contains('Send Gift Card').click()
        cy.wait(1000)
        cy.get('#_sendrecipient_email').type('testerlentes@gmail.com')
        cy.get('#_sendrecipient_name').type('Tu óptica online Colombia')
        cy.get('#_sendstore_id')
            .should('contain', 'Colombia View')
        cy.wait(1000)
        cy.get('#send > .ui-button-text > span').click()
        cy.get('.message-success')
            .should('contain', 'You saved the gift card account.')


            //Ingresar al detalle de la ultima giftcard
        cy.get('.admin__data-grid-wrap tr').eq(2).click()
        cy.get('#_infowebsite_id').should('contain', 'Colombia Website')
        cy.get('.field-state_text > .admin__field-control > .control-value').should('contain', 'Available')
    })
    it('Creación gift card - Chile', () => {
        //////Gift Chile
        cy.wait(1000)
        // Añadir gift nueva
        cy.get('#add > span').click()

        //Information - Completar campos de la gift
        cy.contains('Active').scrollIntoView()
        cy.get('#_infostatus')
            .should('contain', 'Yes')
        cy.get('#_infois_redeemable')
            .should('contain', 'Yes')
        cy.get('#_infowebsite_id').select('Chile Website')
        cy.get('#_infobalance').type('10000')
        cy.wait(1000)

        //Send gift card
        cy.get('[data-ui-id="adminhtml-giftcardaccount-edit-tabs-0-tab-item-send"] > #giftcardaccount_info_tabs_send > :nth-child(1)').contains('Send Gift Card').click()
        cy.wait(1000)
        cy.get('#_sendrecipient_email').type('testerlentes@gmail.com')
        cy.get('#_sendrecipient_name').type('Tu óptica online Chile')
        cy.get('#_sendstore_id')
            .should('contain', 'Chile View')
        cy.wait(1000)
        cy.get('#send > .ui-button-text > span').click()
        //cy.get('.message-success')}.should('contain', 'You saved the gift card account.')
    })
    it('Creación gift card - Argentina', () => {
        //////Gift Argentina
        cy.wait(1000)
        // Añadir gift nueva
        cy.get('#add > span').click()

        //Information - Completar campos de la gift
        cy.contains('Active').scrollIntoView()
        cy.get('#_infostatus')
            .should('contain', 'Yes')
        cy.get('#_infois_redeemable')
            .should('contain', 'Yes')
        cy.get('#_infowebsite_id').select('Argentina Website')
        cy.get('#_infobalance').type('5000')
        cy.wait(1000)

        //Send gift card
        cy.get('[data-ui-id="adminhtml-giftcardaccount-edit-tabs-0-tab-item-send"] > #giftcardaccount_info_tabs_send > :nth-child(1)').contains('Send Gift Card').click()
        cy.wait(1000)
        cy.get('#_sendrecipient_email').type('testerlentes@gmail.com')
        cy.get('#_sendrecipient_name').type('Tu óptica online Argentina')
        cy.get('#_sendstore_id')
            .should('contain', 'Argentina View')
        cy.wait(1000)
        cy.get('#send > .ui-button-text > span').click()
       //cy.get('.message-success')}.should('contain', 'You saved the gift card account.')
    })
    it('Creación gift card - Mexico', () => {
        //////Gift México
        cy.wait(1000)
        // Añadir gift nueva
        cy.get('#add > span').click()

        //Information - Completar campos de la gift
        cy.contains('Active').scrollIntoView()
        cy.get('#_infostatus')
            .should('contain', 'Yes')
        cy.get('#_infois_redeemable')
            .should('contain', 'Yes')
        cy.get('#_infowebsite_id').select('Mexico Website')
        cy.get('#_infobalance').type('2000')
        cy.wait(1000)

        //Send gift card
        cy.get('[data-ui-id="adminhtml-giftcardaccount-edit-tabs-0-tab-item-send"] > #giftcardaccount_info_tabs_send > :nth-child(1)').contains('Send Gift Card').click()
        cy.wait(1000)
        cy.get('#_sendrecipient_email').type('testerlentes@gmail.com')
        cy.get('#_sendrecipient_name').type('Tu óptica online México')
        cy.get('#_sendstore_id')
            .should('contain', 'Mexico View')
        cy.wait(1000)
        cy.get('#send > .ui-button-text > span').click()
        //cy.get('.message-success')}.should('contain', 'You saved the gift card account.')
    })

})