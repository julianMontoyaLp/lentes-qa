/// <reference types="cypress" />

import {username, password} from "../../../fixtures/magentoUser.json"

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

describe('Price Rule + Validacion Front de descuento', () => {

    let cupon = 'CUPON_PRUEBA_CYPRESS'

    it('Creo el price Rule', () => {
        cy.visit('/')
        cy.login(username, password)

        // Marketing > Cart Price Rule
        cy.get('#menu-magento-backend-marketing > [onclick="return false;"]').click()
        cy.get('.item-promo-quote > a').click()
        // Click botón Add New Rule
        cy.get('#add').click()

        // Assert
        cy.get('.page-title')
            .should('contain', 'New Cart Price Rule')
    
        cy.scrollTo(0, 150, {duration: 2000})
        cy.get('[data-index="name"] input[name="name"]').type('b2c_rule_prueba')
        cy.get('[data-index="description"] textarea').type('Se crea un descuento de prueba para B2C en el ambiente')
        cy.scrollTo(0, 450, {duration: 2000})
        cy.get('[data-index="website_ids"] select[name="website_ids"]')
            .select(['Argentina Website', 'Chile Website', 'Colombia Website', 'Mexico Website'])
        
        cy.scrollTo(0, 700, {duration: 2000})
        cy.get('.admin__action-multiselect-text').click()
        cy.get('[data-action="select-all"] > span').click()
        cy.scrollTo(0, 2100, {duration: 4000})
        cy.get('.action-secondary').click()

        cy.scrollTo(0, 1500, {duration: 2000})
        cy.get('[data-index="coupon_type"] select[name="coupon_type"]').select('Specific Coupon')
        
        cy.get('[data-index="coupon_code"] input[name="coupon_code"]').type('CUPON_PRUEBA_CYPRESS')
        
        cy.scrollTo(0, 2000, {duration: 2000})
        cy.get('[data-index="actions"] > .fieldset-wrapper-title > .admin__collapsible-title').click()
        cy.scrollTo(0, 2300, {duration: 3000})
        cy.get('[data-index="actions"] > .admin__fieldset-wrapper-content > .admin__fieldset > ._required input[name="discount_amount"]').clear().type('15')

        cy.get('#save').click()

        cy.get('.message > div').should('contain', 'saved the rule')
        cy.get('.message > div').then(() => {
            cupon = 'CUPON_PRUEBA_CYPRESS'
        })
        cy.get('#promo_quote_grid_filter_coupon_code').type('CUPON_PRUEBA_CYPRESS{enter}')
        cy.get('.even > .col-name')
            .should('exist')
            .and('contain', 'b2c_rule_prueba')
        
        cy.get('.even > .col-coupon_code')
        .should('exist')
        .and('contain', 'CUPON_PRUEBA_CYPRESS')
    }) 

    it('Validacion Front de descuento', () => {
        if(cupon === '') {
            cy.log('Error al crear la orden, no se generó la url de pago')
            // Fuerzo un error en el test
            cy.get('.message')
                .should('contain', 'Fuerzo un error en el test')
        }
        cy.intercept('POST', '/co/checkout/cart/add/uenc/**/product/1135/').as('addProduct')
        cy.intercept('POST', '/co/customer/ajax/login/').as('login')
        cy.intercept('PUT', '/co/rest/co/V1/carts/mine/coupons/CUPON_PRUEBA_CYPRESS').as('cupon')
        // cy.intercept('POST', '/co/mageants_freegift/index/addcoupon').as('addCupon')
        // cy.intercept('POST', '/co/rest/co/V1/carts/mine/totals-information').as('totals')
        // cy.intercept('GET', '/co/rest/co/V1/carts/mine/payment-information?_=**').as('payment')
        // cy.intercept('GET', '/co/customer/section/load/?sections=messages,company&force_new_section_timestamp=true&_=**').as('customer')
        // cy.intercept('POST', '/v1/devices/widgets?referer=https://mc-staging.lentesplus.com').as('devices')
        cy.intercept('GET', '/v1/merchants/pub_test_17lmUJaBD6hDWnfWN8K5RpgcpdYb7FuJ?_=**').as('merchants')
        // cy.intercept('GET', '/jms/lgz/background/etid').as('background')
        // cy.intercept('POST', '/v1/card_tokens?public_key=**&js_version=1.3.1&referer=https://mc-staging.lentesplus.com').as('card_tokens')
        // cy.intercept('GET', '/v1/payment_methods?public_key=**&js_version=1.3.1&locale=es&referer=https://mc-staging.lentesplus.com').as('payment_methods')
        // cy.intercept('GET', '/v1/identification_types?public_key=**&referer=https://mc-staging.lentesplus.com').as('identification_types')
        cy.intercept('DELETE', '/co/rest/co/V1/carts/mine/coupons').as('delete')

        cy.visit('https://mc-staging.lentesplus.com/co')
        cy.get('#search').type('simplus{enter}')
        cy.scrollTo(0, 400, {duration: 3000})
        cy.get('.products > .item > a').click()

        cy.scrollTo(0, 400, {duration: 3000})
        cy.get('#product-addtocart-button').click()
        cy.wait('@addProduct', { requestTimeout: 160000 })
        cy.get('.crosssell-additionals > .action').click()
        cy.scrollTo(0, 100, {duration: 2000})
        cy.get('.methods > :nth-child(2) > .action').click()

        cy.get('.block-content > :nth-child(2) > .action').click()
        cy.get('#email').type('lentest.bruno.prueba@email.com')
        cy.get('.field.password > #pass').type('Bruno123')
        cy.get('#social-form-login > .fieldset > .actions-toolbar > div.primary > #send2').click()
        cy.wait('@login')

        cy.scrollTo(0, 150, {duration: 3000})
        cy.get('#opc-sidebar > .opc-block-summary > :nth-child(4)').click()
        cy.get(':nth-child(4) > .payment-option > .payment-option-content > #discount-form > .payment-option-inner > .field > .control > #discount-code').type(cupon)
        cy.get(':nth-child(4) > .payment-option > .payment-option-content > #discount-form > .text-right > div.primary > .action').click()
        cy.wait(['@cupon'])
        cy.wait(['@merchants'], { requestTimeout: 160000 })

        cy.scrollTo(0, 600, {duration: 4000})
        cy.get('.opc-block-total-in-sidebar > .totals > .mark > .title')
            .should('contain', 'Descuento')
        cy.get('.opc-block-total-in-sidebar > .totals > .amount > .price')
            .should('contain', '-$')

        cy.scrollTo(0, 600, {duration: 4000})
        cy.get(':nth-child(4) > .payment-option > .payment-option-content > #discount-form > .text-right > div.primary > .action').click()
        cy.wait('@delete')
        cy.get(':nth-child(4) > .payment-option > .payment-option-content > .messages > .message > div')
            .should('contain', 'quitado')
        
    })

    it('Borro el cuṕon', () => {
        cy.visit('/')
        cy.login(username, password)

        // Marketing > Cart Price Rule
        cy.get('#menu-magento-backend-marketing > [onclick="return false;"]').click()
        cy.get('.item-promo-quote > a').click()

        cy.get('#promo_quote_grid_filter_coupon_code').type('CUPON_PRUEBA_CYPRESS{enter}')
        cy.get('.even > .col-name')
            .should('exist')
            .and('contain', 'b2c_rule_prueba')
            .click()
        
        cy.get('#delete').click()
        cy.get('.action-primary').click()
        cy.get('.message > div')
            .should('contain', 'You deleted the rule')
        cy.get('.empty-text')
            .should('exist')
    })
})