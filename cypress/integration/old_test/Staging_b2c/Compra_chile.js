/*Ingresar a Lentesplus
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})
it('finds the content "type"', () => {
    cy.visit('http://mc-staging.lentesplus.com/cl')
    
    //Click en Botón de login
    cy.get('[data-label="ó"] > .lpi-0002-user').click() 
    
    //Click en ya tengo cuenta 
    cy.get('.mfp-content').contains('Ya tengo cuenta Lentesplus').click()
    
    //Ingresar Datos de ingreso
    cy.get('.form-login').get('#email').type("sebastian.cortes@lentesplus.com")
    cy.get('.form-login').get('#pass').type("Lentes2020")
    cy.get('.form-login').get('#send2').click()
    
    //Click en recomprar dentro de mi cuenta.
    cy.wait(8000)
    cy.get(':nth-child(1) > .actions > .order > span').click()
    
    //click en finalizar compra 
    cy.wait(6000)
    cy.get('.crosssell-additionals > .action').click()
    cy.get('.methods > :nth-child(2) > .action > span').click()
    
    //Click en continuar primer paso del checkout 
    cy.get(':nth-child(4) > .button > span').click()
    
    //Finalizar Compra Checkout
        cy.wait(6000)
        cy.get('#cardNumber').type("4594260325486367")
        cy.wait(3000)
        cy.get('#cardExpirationMonth').select('9')
        cy.get('#cardExpirationYear').select('2024')
        cy.get('#securityCode').type("975")
        cy.get('#cardholderName').type("Diego Marino Locust SAS")
        cy.get('#docType').select('Otro')
        cy.get('#docNumber').type("1019086995")
        cy.get('#installments').select('1')
        cy.get('#mp-custom-save-payment').click()
    })
  */
    