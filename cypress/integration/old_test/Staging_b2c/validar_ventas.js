/// <reference types="cypress" />

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

describe('Validar seccion de ventas - en Staging - ', () => {

    beforeEach(() => {
        cy.visit('/')
        cy.login('bruno.dionel', 'Lentes2020*')
    })

    it('View Sale expirada', () => {
        // Validar Orden Creada expirada
        // Sales-orders
        cy.get('#menu-magento-sales-sales > [onclick="return false;"]').click()
        // Sales - Orders
        cy.get('.item-sales-order > a > span').click()
        cy.wait(3000)
        cy.get('[data-bind="afterRender: $data.setToolbarNode"] > :nth-child(1) > .data-grid-search-control-wrap > #fulltext').clear();
        cy.get('[data-bind="afterRender: $data.setToolbarNode"] > :nth-child(1) > .data-grid-search-control-wrap > #fulltext').type('300267454');
        cy.get('[data-bind="afterRender: $data.setToolbarNode"] > :nth-child(1) > .data-grid-search-control-wrap > .action-submit').click();
        cy.get('[data-role="grid-wrapper"]').contains('td','300267454');

        //Ingresar al detalle de la orden
        cy.get('[data-role="grid-wrapper"]').contains('td','300267454').click();

    });
    it('View Sale confirmada con invoice', () => {
            // validar una orden confirmada

        // Sales-orders
        cy.get('#menu-magento-sales-sales > [onclick="return false;"]').click()
        // Sales - Orders
        cy.get('.item-sales-order > a > span').click()
        cy.wait(3000)
        cy.get('[data-bind="afterRender: $data.setToolbarNode"] > :nth-child(1) > .data-grid-search-control-wrap > #fulltext').clear();
        cy.get('[data-bind="afterRender: $data.setToolbarNode"] > :nth-child(1) > .data-grid-search-control-wrap > #fulltext').type('500161758');
        cy.get('[data-bind="afterRender: $data.setToolbarNode"] > :nth-child(1) > .data-grid-search-control-wrap > .action-submit').click();
        cy.get('[data-role="grid-wrapper"]').contains('td','500161758').click();
        cy.get(':nth-child(2) > td > #order_status').should('have.text', 'Confirmado')
        cy.get('[data-ui-id="sales-order-tabs-tab-item-order-invoices"] > #sales_order_view_tabs_order_invoices > :nth-child(1)').click();
        cy.get('#sales_order_view_tabs_order_invoices_content').contains('td','500161758').click();
        cy.get('.order-payment-method-title > .data-table > tbody > :nth-child(5) > td').should('have.text', 'APROBADA - Autorizada');

    });
    it('View Sale con shipment', () => {
        // validar una orden con shipments
        // Sales-orders
        cy.get('#menu-magento-sales-sales > [onclick="return false;"]').click()
        // Sales - Orders
        cy.get('.item-sales-order > a > span').click()
        cy.wait(3000)
        cy.get('[data-bind="afterRender: $data.setToolbarNode"] > :nth-child(1) > .data-grid-search-control-wrap > #fulltext').clear();
        cy.get('[data-bind="afterRender: $data.setToolbarNode"] > :nth-child(1) > .data-grid-search-control-wrap > #fulltext').type('1900000379');
        cy.get('[data-bind="afterRender: $data.setToolbarNode"] > :nth-child(1) > .data-grid-search-control-wrap > .action-submit').click();
        cy.get('[data-role="grid-wrapper"]').contains('td','1900000379').click();
        cy.get(':nth-child(2) > td > #order_status').should('have.text', 'Envió')
        cy.get('[data-ui-id="sales-order-tabs-tab-item-order-shipments"] > #sales_order_view_tabs_order_shipments > :nth-child(1)').click();
        cy.get('#sales_order_view_tabs_order_shipments_content').contains('td','1900000379').click();
        cy.get('#order_status').should('have.text', 'Envió');
    });
})
