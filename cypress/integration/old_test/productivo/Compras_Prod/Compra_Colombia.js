Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})
//Ingresar a Lentesplus
it('finds the content "type"', () => {
cy.visit('https://lentesplus.com/co/')
//Click en Botón de login
cy.get('.reorder-login').contains('Re-comprar').click()

//Click en ya tengo cuenta
cy.get('.mfp-content').contains('Ya tengo cuenta Lentesplus').click()

//Ingresar Datos de ingreso
cy.get('.form-login').get('#email').type("sebastian.cortes@lentesplus.com")
cy.get('.form-login').get('#pass').type("Lentes2020")
cy.get('.form-login').get('#send2').click()
cy.wait(8000)

// Buscar producto desde el admin
cy.get('#search').type("23BLRE103").type('{enter}')
// Click al producto 
cy.get('.product-item-info').click()

// Click en agregar a la bolsa 

cy.get('#product-addtocart-button').click()

//click en finalizar compra
cy.wait(5000)
cy.get('.crosssell-additionals > .action').click()
cy.wait(25000)
cy.get('.methods > :nth-child(2) > .action > span').click()

//Click en continuar primer paso del checkout
cy.get(':nth-child(4) > .button > span').click()

//Ingresar tarjeta
cy.wait(8000)
cy.get('#wompi_cc_number').type("4594260325486367")
cy.get('#wompi_expiration').select('9')
cy.get('#wompi_expiration_yr').select('2024')
cy.get('#wompi_cc_cid').type("975")
cy.get('#wompi_cc_holder').type("Diego marino Locust sas")
cy.get('#wompi_installments').select('1')
cy.wait(3000)
cy.get('#payment_form_wompi > div:nth-child(7) > label > em').click()
cy.get('[style="width: 617px; margin-left: -215px; top: 652px;"] > div.primary > .action > span').click()
})


