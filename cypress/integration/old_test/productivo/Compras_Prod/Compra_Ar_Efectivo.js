Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
  })

//Ingresar a Lentesplus
it('finds the content "type"', () => {
    cy.visit('https://www.lentesplus.com/ar/')

    //Click en Botón de login
    cy.get('.reorder-login').contains('Re-comprar').click()

    //Click en ya tengo cuenta
    cy.get('.mfp-content').contains('Ya tengo cuenta Lentesplus').click()

    //Ingresar Datos de ingreso
    cy.get('.form-login').get('#email').type("sebastian.cortes@lentesplus.com")
    cy.get('.form-login').get('#pass').type("Lentes2020")
    cy.get('.form-login').get('#send2').click()
    cy.wait(6000)

  // Buscar producto desde el admin
   cy.get('#search').type("BioTrue 120ml").type('{enter}')
  // Click al producto
  cy.get(':nth-child(1) > a > .product-item-info').click()

  // Click en agregar a la bolsa

    cy.get('#product-addtocart-button').click()

  //click en finalizar compra

    //cy.get('.crosssell-additionals > .action').click()
    cy.get('.methods > :nth-child(2) > .action > span').click()
    cy.wait(2500)
  //Click en continuar primer paso del checkout
    cy.wait(2500)
    cy.get('[data-bind="submit: navigateToNextStep"] > .actions-toolbar > div.primary > .button').click()

    //Direccion default
    cy.get(':nth-child(4) > .button').click()
    //Seleccionar pago en efectivo

    cy.wait(5000)
    cy.get('.mercadopago_customticket > .payment-method-title').click()

    cy.get('#mercadopago-payment-methods-ticket > li:nth-child(2) > label').click()

    cy.get('[style="width: 617px; margin-left: -215px; top: 384px;"] > div.primary > .action').click()

    })


