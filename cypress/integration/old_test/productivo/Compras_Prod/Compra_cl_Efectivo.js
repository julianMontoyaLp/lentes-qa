//Ingresar a Lentesplus
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})
it('finds the content "type"', () => {
    cy.visit('http://lentesplus.com/cl')
    
    //Click en Botón de login
    cy.get('[data-label="ó"] > .lpi-0002-user').click() 
    
    //Click en ya tengo cuenta 
    cy.get('.mfp-content').contains('Ya tengo cuenta Lentesplus').click()
    
    //Ingresar Datos de ingreso
    cy.get('.form-login').get('#email').type("sebastian.cortes@lentesplus.com")
    cy.get('.form-login').get('#pass').type("Lentes2020")
    cy.get('.form-login').get('#send2').click()
    cy.wait(10000)

     // Buscar producto desde el admin
   cy.get('#search').type("33BLBT102").type('{enter}')
   // Click al producto 
    cy.get('.product-item-info').click()
 
   // Click en agregar a la bolsa 
 
     cy.get('#product-addtocart-button').click() 
    
    //click en finalizar compra 
    cy.wait(5000)
    cy.get('.crosssell-additionals > .action').click()
    cy.wait(25000)
    cy.get('.methods > :nth-child(2) > .action > span').click()
    
    //Click en continuar primer paso del checkout 
    cy.get(':nth-child(4) > .button > span').click()
    
    //Finalizar Compra Checkout
    cy.wait(12000)
    cy.get('#checkout-payment-method-load > div > div.payment-group > div.mercadopago_customticket.payment-method > div.payment-method-title.field.choice').click()
    cy.get('[style="width: 617px; margin-left: -215px; top: 228px;"] > div.primary > .action').click()
    })
    
    