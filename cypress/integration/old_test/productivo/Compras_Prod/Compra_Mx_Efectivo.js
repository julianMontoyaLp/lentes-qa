//Ingresar a Lentesplus
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
  })
it('finds the content "type"', () => {
    cy.visit('https://www.lentesplus.com/mx/')
    
    //Click en Botón de login
    cy.get('[data-label="ó"] > .lpi-0002-user').click() 
    
    //Click en ya tengo cuenta 
    cy.get('.mfp-content').contains('Ya tengo cuenta Lentesplus').click()
    
    //Ingresar Datos de ingreso
    cy.wait(2000)
    cy.get('.form-login').get('#email').type("sebastian.cortes@lentesplus.com")
    cy.get('.form-login').get('#pass').type("Lentes2020")
    cy.get('.form-login').get('#send2').click()
    cy.wait(8000)

      // Buscar producto desde el admin
   cy.get('#search').type("43ALOF108").type('{enter}')
   // Click al producto 
    cy.get('.product-item-info').click()
    cy.wait(4000)
 
   // Click en agregar a la bolsa 
 
     cy.get('#product-addtocart-button').click() 
     cy.wait(35000)
    //Click en Finalizar compra 
    cy.get('.methods > :nth-child(2) > .action > span').click()
    

    //Click en continuar 
    cy.get(':nth-child(4) > .button').click()
    //Click en Facturama 
    cy.wait(20000)
    cy.get('#facturama-form > .actions-toolbar > div.primary > .button > :nth-child(1) > span').click()
    //Credenciales tarjeta 

    cy.wait(6000)
    cy.get('.mercadopago_customticket > .payment-method-title').click()
    cy.get('#mercadopago-payment-methods-ticket > li:nth-child(1) > label').click()
    cy.get('[style="width: 617px; margin-left: -215px; top: 378px;"] > div.primary > .action').click()

    })

    
    