//Ingresar a Lentesplus
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
  })
//Ingresar a Lentesplus
it('finds the content "type"', () => {
    cy.visit('https://lentesplus.com/co/')
    
    //Click en Botón de login
    cy.get('.reorder-login').contains('Re-comprar').click()
    
    //Click en ya tengo cuenta
    cy.get('.mfp-content').contains('Ya tengo cuenta Lentesplus').click()
    
    //Ingresar Datos de ingreso
    cy.get('.form-login').get('#email').type("sebastian.cortes@lentesplus.com")
    cy.get('.form-login').get('#pass').type("Lentes2020")
    cy.get('.form-login').get('#send2').click()

    cy.wait(8000)

    //Click en los pedidos 

    cy.get('.lpi-0080-order').click()
    cy.scrollTo('bottom', {duration: 5000})

    //Click en mi cuenta 

    cy.get('.lpi-0081-data').click()
    cy.scrollTo('bottom', {duration: 5000})

    //Click en direcciones 

    cy.get('.lpi-0082-direction').click()
    cy.scrollTo('bottom', {duration: 5000})

    // Click en mis tarjetas

    cy.get('.lpi-0053-creditCard')
    cy.scrollTo('bottom', {duration: 5000})

    // Click en Formula 

    cy.get('.lpi-0021-prescription > span')
    cy.scrollTo('bottom', {duration: 5000})

    //

})