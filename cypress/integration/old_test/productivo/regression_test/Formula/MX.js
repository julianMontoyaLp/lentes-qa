//Ingresar a Lentesplus
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
  })
//Ingresar a Lentesplus
it('finds the content "type"', () => {
    cy.visit('https://lentesplus.com/mx/')
    
    //Click en Botón de login
    cy.get('.reorder-login').contains('Re-comprar').click()
    
    //Click en ya tengo cuenta
    cy.get('.mfp-content').contains('Ya tengo cuenta Lentesplus').click()
    
    //Ingresar Datos de ingreso
    cy.get('.form-login').get('#email').type("sebastian.cortes@lentesplus.com")
    cy.get('.form-login').get('#pass').type("Lentes2020")
    cy.get('.form-login').get('#send2').click()

    cy.wait(8000)

    // Click en Formula 

    cy.get(':nth-child(5) > .lpi-0021-prescription').click()
    cy.scrollTo('bottom', {duration: 5000})

    //

    cy.get('.add-formula > div').click()

    // agregar formula nueva 

    cy.get('#description').type("Pruebas Robot")

    // upload Image

    it('Testing picture uploading', () => {
        cy.fixture('testPicture.png').then(fileContent => {
            cy.get('.fileContainer > #image').attachFile({
                fileContent: fileContent.toString(),
                fileName: 'testPicture.png',
                mimeType: 'image/png'
            });
        });
    });


    //

})