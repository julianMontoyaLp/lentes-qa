//Ingresar a Lentesplus
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})
//Ingresar a Lentesplus pwa
it('finds the content "type"', () => {
    cy.visit('https://lentesplus.com/cl')

// Click Login desde el Home 

cy.get('[data-label="ó"] > .lpi-0002-user').click()

// validacion de tamaño por css 


cy.get('.block-content > :nth-child(2) > .action').wrap({ width: 320 }).its('width') 


// Click Login Todos los lentes 

cy.get('.mfp-close').click()
cy.get('#ui-id-6').click()
cy.get('[data-label="ó"] > .lpi-0002-user').click()
cy.wait(2000)
cy.get('.mfp-close').click()


//Click desde la ficha del producto 
cy.wait(5000)
cy.get('#search').type("33BLBT102").type('{enter}')
cy.get('.product-item-info').click()
cy.get('[data-label="ó"] > .lpi-0002-user').click()
cy.get('.mfp-close').click()

// Click en agregar a la bolsa 

cy.get('#product-addtocart-button').click()

//click en finalizar compra
cy.wait(8000)
cy.get('.crosssell-additionals > .action').click()
cy.get('.methods > :nth-child(2) > .action > span').click()

    })