//Ingresar a Lentesplus
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})
import catalogoAR from '../../fixtures/testspwa/catalogoAR.json'
import catalogoCO from '../../fixtures/testspwa/catalogoCO.json'
import catalogoCL from '../../fixtures/testspwa/catalogoCL.json'
import catalogoMX from '../../fixtures/testspwa/catalogoMX.json'
describe('B2C - Catalogo - ficha de prducto', () => {
    //Ingresar a Lentesplus pwa

    it('B2C - Catalogo ', () => {
        cy.viewport(1280, 900);
        cy.visit(Cypress.env('urlb2c'));
        let Catalogo = ''
        if(Cypress.env('urlb2c').includes("lentesplus.com/ar")  ){
            Catalogo = catalogoAR
        }
        if(Cypress.env('urlb2c').includes("lentesplus.com/co") ){
            Catalogo = catalogoCO
        }
        if(Cypress.env('urlb2c').includes("lentesplus.com/cl") ){
            Catalogo = catalogoCL
        }
        if(Cypress.env('urlb2c').includes("lentesplus.com/mx") ){
            Catalogo = catalogoMX
        }
        cy.get(Catalogo.link_menu).click()
        cy.wait(2000)
        cy.scrollTo('bottom', {duration: 3000})
        cy.wait(2000)
        cy.scrollTo('bottom', {duration: 3000})
        cy.get("[data-product-id="+Catalogo.producto_busqueda+"] > a").click();
        //53ALOF104
        //cy.get('#b826989b-9851-41c3-931d-192c22187b2a')

        cy.get('.productFullDetail-productName-OXW').should('contain',Catalogo.producto_titulo);

        if(cy.get('.detailsTable-detailOptions-1ii').length > 0){
            cy.get('.detailsTable-detailOptions-1ii > :nth-child(2)').click();
            cy.get('.detailsTable-selectedOption-1mb').click();
            cy.get('.detailsTable-detailOptions-1ii > :nth-child(2)').click();
            cy.get('.detailsTable-techUl-3lu').should('contain', Catalogo.producto_SKU)
        }

        //Validacion de botones de cantidad
        if(cy.get('.pdpqty-qtyButtons-3bu').length > 0){
            cy.get('.pdpqty-qtyButtons-3bu > :nth-child(2)').should('contain', "1")
            cy.get('.pdpqty-qtyButtons-3bu > :nth-child(3)').click();
            cy.get('.pdpqty-qtyButtons-3bu > :nth-child(2)').should('contain', "2")
        }
            //Validacion de valores en la tabla de detalle
        //cy.get('.deliveryTimes-fieldInput-29i.').select('Aquitania, Boyacá').
        cy.get("body").then($body => {
            if ($body.find("detailsTable-detailOptions-1ii > :nth-child(3)").length > 0) {
                cy.get('.detailsTable-detailOptions-1ii > :nth-child(3)').click();
                cy.get('.detailsTable-reviewsDataMainLeftNumber-3uh > :nth-child(1)').should('exist')
                cy.get('.detailsTable-reviewsListBottom-3bh > :nth-child(1)').should('exist')
            }
        });
        //cy.get('.productFullDetail-productsSliderTitle-1BO').should('contain', "También te puede interesar")
    })


})
