// import user from '../../fixtures/b2cUser.json'
import user from '../../fixtures/b2cUser.json'
import magentoUser from '../../fixtures/magentoUser.json'
import { aliasQuery } from '../../support/graphql-test-utils'

//Ingresar a Lentesplus
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

describe('B2C - Cancelaciones', () => {
  if(Cypress.env('urlb2c').includes("lentesplus.com/cl")) return
  let nroOrden;

  beforeEach(() => {
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql`, (req) => {
      // Queries
      console.log(req.body)
      aliasQuery(req,'createCart')
      aliasQuery(req,'signIn')
      aliasQuery(req,'mergeCarts')
      aliasQuery(req, 'getProductsBySku')
      aliasQuery(req, 'addSimpleProductToCart')
      aliasQuery(req, 'SetShippingMethod')
      aliasQuery(req, 'resolveWompiOrderStatus')
      aliasQuery(req, 'placeOrder')
      aliasQuery(req, 'setSelectedPaymentMethod')
      aliasQuery(req, 'setBillingAddress')
      aliasQuery(req, 'SetCustomerAddressOnCart')
    })

    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getLocale**`).as('getLocale')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+checkUserIsAuthed**`).as('checkUserIsAuthed')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+cmsBlocks**`).as('cmsBlocks')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getKeys**`).as('userPopup')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getRootCategoryId**`).as('yaTengoCuentaBtn')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+accountChipQuery**`).as('accountChipQuery')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getItemCount**`).as('getItemCount')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getCustomer**`).as('getCustomer')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getCartDetails**`).as('getCartDetails')

    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+productSearch**`).as('productSearch')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getProductFiltersBySearch**`).as('getProductFiltersBySearch')

    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getProductDetailForProductPage**`).as('getProductDetailForProductPage')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getProductRatingInfoQuery**`).as('getProductRatingInfoQuery')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GetBreadcrumbs**`).as('GetBreadcrumbs')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getIsDisableProductoLenQuery**`).as('getIsDisableProductoLenQuery')

    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GetCartDetails**`).as('GetCartDetailsQuery')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getPriceSummary**`).as('getPriceSummary')

    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getProductsBySku**`).as('dataCarrouselCart')

    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getProductListing**`).as('getProductListing')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GetCustomer**`).as('GetCustomer')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+deliveryTimeCart**`).as('deliveryTimeCart')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getSelectedShippingAddress**`).as('getSelectedShippingAddress')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GetShippingInformation**`).as('GetShippingInformation')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GetCustomerAddresses**`).as('GetCustomerAddresses')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GetCustomerCartAddress**`).as('GetCustomerCartAddress')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getItemsInCart**`).as('getItemsInCart')

    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getPaymentInformation**`).as('getPaymentInformation')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=%7BcustomerPaymentTokens**`).as('customerPaymentTokens')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getOrderDetails**`).as('getOrderDetails')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getGeneralRatingInfo**`).as('getGeneralRatingInfo')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getMercadoPagoResult**`).as('getMercadoPagoResult')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getOrderDetailsLen**`).as('getOrderDetailsLen')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getShippingTime**`).as('getShippingTime')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getSelectedAndAvailableShippingMethods**`).as('getSelectedAndAvailableShippingMethods')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getPointCart**`).as('getPointCart')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getStatusPlusUp**`).as('getStatusPlusUp')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GET_LOYALTY_LEVEL**`).as('getLoyaltyLevel')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GET_CONFIG_LENTES_SHIPPING**`).as('getConfigLentesShipping')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getSelectedAndAvailableShippingMethods**`).as('getSelectedAndAvailableShippingMethods')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+cart**`).as('getCart')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getAppliedCoupons**`).as('getAppliedCoupons')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GetCustomerOrders**`).as('getCustomerOrders')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GetCustomerOrdersPaginated**`).as('getCustomerOrdersPaginated')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getRMAStoreConfig**`).as('getRMAStoreConfig')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GET_CUSTOMER_SUSPENDED_SUBSCRIPTIONS**`).as('getCustomerSuspendedSubscriptions')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GET_CUSTOMER_ACTIVE_SUBSCRIPTIONS**`).as('getCustomerActiveSubscriptions')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GetCustomerInformation**`).as('getCustomerInformation')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GetProductThumbnailsByURLKey**`).as('getProductThumbnailsByURLKey')

  })

    //Ingresar a Lentesplus pwa
  it('Test B2C compra efectivo', () => {
    cy.log(user)
    cy.viewport(1280, 900);

    // Pra este test, tener en cuenta que la tienda en Chile no dispone de Pagos en Efectivo
    if(Cypress.env('urlb2c').includes("lentesplus.com/cl")) return

    cy.visit(Cypress.env('urlb2c'));

    // Click user pop up
    cy.wait('@getLocale')
    cy.wait('@checkUserIsAuthed')
    cy.wait('@cmsBlocks')
    cy.get('.headerDesktop-lpi_0002_user-2RI', { timeout: 20000 }).click()
    cy.wait('@userPopup')
    cy.wait(5000)

    // Click YA TENGO CUENTA LENTESPLUS
    cy.get('.login-tengoCuenta-3uc').click({force: true})
    cy.wait('@yaTengoCuentaBtn')
    cy.get('.signIn-form-1sq input[name="email"]').type(user.email, {force: true})
    cy.get('.signIn-form-1sq input[name="password"]', { timeout: 10000 }).type(user.password, { timeout: 30000 })
    cy.get('.signIn-ingresar-3Oc').click()
    cy.wait('@gqlsignInQuery')
    // cy.wait('@accountChipQuery')
    cy.wait('@gqlcreateCartQuery')
    cy.wait('@getItemCount')
    cy.wait('@gqlmergeCartsQuery')
    cy.wait('@getCustomer')
    cy.wait('@getCartDetails')

    cy.get('.successDiv-headerText-2xN > span')
      .should('contain', 'Inicio de sesión exitoso')
    cy.get('.successDiv-infoText-3UQ > :nth-child(1)')
      .should('contain', 'Hola')
    cy.get('.successDiv-infoText-3UQ > :nth-child(2)')
      .should('contain', '¡Qué bueno verte!')

    // Click en el Continuar del Login Modal una vez loueado
    cy.get('.successDiv-continuar-2ev').click()

    // Ingreso el sku del lente a buscar según store
    if(Cypress.env('urlb2c').includes("lentesplus.com/ar")) {
      cy.get('.headerDesktop-searchBar-2K2 input[name="search_query"]', { timeout: 10000 }).type(`51ALAO107{enter}`)
    } else if(Cypress.env('urlb2c').includes("lentesplus.com/co")) {
      cy.get('.headerDesktop-searchBar-2K2 input[name="search_query"]', { timeout: 10000 }).type(`21ALAO106{enter}`)
    } else if(Cypress.env('urlb2c').includes("lentesplus.com/mx")){
      cy.get('.headerDesktop-searchBar-2K2 input[name="search_query"]', { timeout: 10000 }).type(`41ALAO107{enter}`)
    }

    cy.wait('@productSearch')
    // cy.wait('@getProductFiltersBySearch')

    cy.get('.searchPage-categoryTitle-272')
      .should('contain', 'Resultados de búsqueda para:')

    cy.get('.searchPage-totalProducts-3pc > span > strong')
      .then(($quantity) => {
        cy.wrap(Number($quantity.text()))
          .should('be.gte', 1)
      })

    // Click en el producto
    cy.get('.item-root-1s5 > a').click()
    cy.wait('@getProductDetailForProductPage')
    cy.wait('@getProductRatingInfoQuery')
    cy.wait('@GetBreadcrumbs')

    cy.get('.productFullDetail-productName-OXW')
      .should('contain', 'AIR OPTIX Colors Neutros')

    cy.get('.pdpglasses-mainContainer-3TY > :nth-child(2) select', { timeout: 10000 }).select('Miel')
    // cy.wait('@getIsDisableProductoLenQuery')

    cy.get('.productFullDetail-buttonEnabled-3xi').click()
    cy.wait('@gqladdSimpleProductToCartQuery')
    cy.wait('@getCartDetails')
    cy.wait('@getProductDetailForProductPage')
    cy.wait('@GetCartDetailsQuery')
    cy.wait('@getPriceSummary')
    cy.wait('@dataCarrouselCart')

    // Click en No, gracias del Cross Sell
    cy.get('.crosssell-closeButton-1vV').click()

    cy.get('.cartPage-successAlert-UJI > span')
      .should('contain', 'Añadiste AIR OPTIX Colors Neutros a tu carrito de compras.')

    // Click en Finalizar Compra
    cy.get('.priceSummary-buttonFilled-a57').click()
    cy.wait(5000)
    cy.wait('@getProductListing')
    cy.wait('@GetCustomer')
    cy.wait('@deliveryTimeCart')
    cy.wait('@getSelectedShippingAddress')
    cy.wait('@GetShippingInformation')
    cy.wait('@getPriceSummary')
    cy.wait('@GetCustomerAddresses')
    cy.wait('@GetCustomerCartAddress')
    cy.wait('@getItemsInCart')
    cy.wait('@getSelectedAndAvailableShippingMethods')
    cy.wait('@GetCustomer')
    if(Cypress.env('urlb2c').includes("lentesplus.com/mx")){
      cy.wait(5000)
    }
    cy.wait(5000)
    cy.get('.checkoutPage-heading-sU7', { timeout: 20000 })
      .should('contain', 'Finaliza tu compra')

    if(Cypress.env('urlb2c').includes("lentesplus.com/ar")) {
      cy.get('input[value="lentesshipping|lentesshipping"]').click({force: true})
    } else if(Cypress.env('urlb2c').includes("lentesplus.com/co")) {
      cy.get('input[value="freeshipping|freeshipping"]').click({force: true})
    }
    cy.wait(2000)
    cy.get('.checkoutPage-review_order_button-3B5',{ timeout: 10000 }).click({force: true})
    cy.wait('@getPaymentInformation')
    cy.wait('@customerPaymentTokens')

    if(Cypress.env('urlb2c').includes("lentesplus.com/ar")) {
      cy.get(':nth-child(4) > .radio-root-112').click()
      cy.get(':nth-child(1) > :nth-child(3) > .mercadoPagoRadio-root-1L6').click()
    } else if(Cypress.env('urlb2c').includes("lentesplus.com/co")) {
      cy.get(':nth-child(4) > .radio-root-112').click()
      cy.get(':nth-child(1) > :nth-child(3) > .mercadoPagoRadio-root-1L6').click()
    } else if(Cypress.env('urlb2c').includes("lentesplus.com/mx")){
      cy.get(':nth-child(4) > .radio-root-112').click()
      cy.wait(2000)
      cy.get(':nth-child(5) > :nth-child(3) > .mercadoPagoRadio-root-1L6 > .mercadoPagoRadio-imageContainer-261 > .mercadoPagoRadio-loaded-WR5').click()
    }

    // Click Continuar de Métodos de Pago
    cy.get('.checkoutPage-review_order_button-3B5').click()
    cy.wait(5000)
    cy.wait('@gqlsetBillingAddressQuery')
    cy.wait('@gqlsetSelectedPaymentMethodQuery')
    cy.wait('@getOrderDetails')
    cy.wait('@gqlplaceOrderQuery')
    cy.wait('@getGeneralRatingInfo')
    cy.wait('@gqlcreateCartQuery')
    cy.wait('@getMercadoPagoResult')

    cy.get('.orderConfirmationPage-orderNumberConfirm-3kC')
      .should('contain', 'PAGO PENDIENTE EFECTIVO')
    cy.get('.orderConfirmationPage-paymentMethodName-2H7 > span')
      .should('contain', 'Pago En Efectivo')

    cy.get('.shippingAddress-title-1jC')
      .should('contain', 'Dirección de envío')

    // Obtengo Nro de Orden
    cy.get('.orderConfirmationPage-orderNumber-2YL strong > a').then(($number) => {
      nroOrden = $number.text().slice(16)
      cy.log(nroOrden)
    })
  })

  it('Ingresa al admin > pasa orden de Pago Pendiente a Confirmado', () => {
    // Request Magento
    cy.intercept('GET', `${Cypress.env('urlb2c_base')}/admin/mui/index/render/key/**/?namespace=notification_area&sorting%5Bfield%5D=created_at&sorting%5Bdirection%5D=asc&isAjax=true`).as('getSortGrid')

    cy.intercept('GET', `${Cypress.env('urlb2c_base')}/admin/mui/index/render/key/**/?namespace=sales_order_grid**`).as('getOrderGrid')

    cy.intercept('GET', `${Cypress.env('urlb2c_base')}/admin/ordereditor/form/load/key/**/`).as('getOrderEditor')

    cy.intercept(`${Cypress.env('urlb2c_base')}/admin/ordereditor/form/load/key/**/`).as('postOrderEditor')

    cy.log(nroOrden)
    cy.visit('https://mc-staging.lentesplus.com/admin')
    cy.get('#username').type(magentoUser.username)
    cy.get('#login').type(magentoUser.password)
    cy.get('.action-login').click()

    cy.get('.page-title')
      .should('contain', 'Dashboard')

    cy.get('.admin-user-account-text')
      .should('contain', magentoUser.username)

    cy.get('#menu-magento-sales-sales > [onclick="return false;"]').click()
    cy.get('.item-sales-order > a').click()

    cy.get('.page-title')
      .should('contain', 'Orders')

    cy.wait('@getSortGrid')

    cy.get('[data-bind="afterRender: $data.setToolbarNode"] > :nth-child(1) > .data-grid-search-control-wrap > #fulltext').clear().type(`${nroOrden}{enter}`)

    cy.wait('@getOrderGrid')

    cy.get('.data-grid-actions-cell > .action-menu-item').click()
    cy.get('.page-title')
      .should('contain', `#${nroOrden}`)

    cy.get('#ordereditor-info-link > a').click()
    cy.wait('@postOrderEditor')

    cy.get('#status').select('confirmed')
    cy.get('#info-submit').click()
    cy.wait(10000)
    cy.get(':nth-child(2) > td > #order_status', { timeout: 20000 })
      .should('contain', 'Confirmado')
  })

  it('Cancela orden en lentesplus', () => {
    cy.viewport(1280, 900);

    // Pra este test, tener en cuenta que la tienda en Chile no dispone de Pagos en Efectivo
    if(Cypress.env('urlb2c').includes("lentesplus.com/cl")) return

    cy.visit(Cypress.env('urlb2c'));

    // Click user pop up
    cy.wait('@checkUserIsAuthed')
    cy.wait('@cmsBlocks')
    cy.get('.headerDesktop-lpi_0002_user-2RI', { timeout: 20000 }).click()
    cy.wait('@userPopup')
    cy.wait(5000)

    // Click YA TENGO CUENTA LENTESPLUS
    cy.get('.login-tengoCuenta-3uc').click({force: true})
    cy.wait('@yaTengoCuentaBtn')
    cy.get('.signIn-form-1sq input[name="email"]').type(user.email, {force: true})
    cy.get('.signIn-form-1sq input[name="password"]', { timeout: 10000 }).type(user.password, { timeout: 30000 })
    cy.get('.signIn-ingresar-3Oc').click()
    cy.wait('@gqlsignInQuery')
    // cy.wait('@accountChipQuery')
    cy.wait('@gqlcreateCartQuery')
    cy.wait('@getItemCount')
    cy.wait('@gqlmergeCartsQuery')
    cy.wait('@getCustomer')
    cy.wait('@getCartDetails')

    cy.get('.successDiv-headerText-2xN > span')
      .should('contain', 'Inicio de sesión exitoso')
    cy.get('.successDiv-infoText-3UQ > :nth-child(1)')
      .should('contain', 'Hola')
    cy.get('.successDiv-infoText-3UQ > :nth-child(2)')
      .should('contain', '¡Qué bueno verte!')

    // Click en el Continuar del Login Modal una vez logueado
    cy.get('.successDiv-continuar-2ev').click()

    cy.wait('@getRMAStoreConfig')
    cy.wait('@getCustomerSuspendedSubscriptions')
    cy.wait('@getCustomerActiveSubscriptions')
    cy.wait('@getCustomerOrders')
    cy.wait('@getCustomerInformation')
    cy.wait('@getCustomerOrdersPaginated')
    cy.wait('@getProductThumbnailsByURLKey')

    cy.get('.accountEdit-titleContainer-3bs > div > :nth-child(1)')
      .should('contain', '¡Qué bueno verte!')

    cy.get(':nth-child(2) > .orderSummary-orderContainer-23N > .orderSummary-orderRowHeader-3Cq > .orderSummary-orderNumber-1Ul > :nth-child(2)')
      .should('contain', nroOrden)

    cy.get(':nth-child(2) > .orderSummary-orderContainer-23N > .orderSummary-orderRowDetails-2Sh > .orderSummary-orderProductsAndActions-1qF > .orderSummary-buttonsContainer-1Ve > .orderSummary-buttonCancelOrChange-2aN').click()
    cy.get('.orderSummary-confirmDialogActions-3OC > :nth-child(1)').click()

    cy.get('.RMAPage-orderDetailsCardNumber-3Cp')
      .should('contain', nroOrden)

    cy.get('.RMAPage-selectAll-1L3').click()
    cy.get('.RMAPage-nextButton-3xo > .button-content-3je').click()

    cy.get('select').select('No estoy de acuerdo con el tiempo de entrega')
    cy.get(':nth-child(1) > .RMAPage-returnMethodLabel-3OR').click()
    cy.get('.RMAPage-nextButton-3xo > .button-content-3je').click()
    cy.get('.RMAPage-nextButton-3xo > .button-content-3je').click()

    cy.get('.RMAPage-confirmDialogActions-3Dt button:nth-child(1)').click()
  })

})