// import user from '../../fixtures/b2cUser.json'
import user from '../../fixtures/b2cUser.json'
import magentoUser from '../../fixtures/magentoUser.json'
import { aliasQuery } from '../../support/graphql-test-utils'

//Ingresar a Lentesplus
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

describe('B2C - Plusup', () => {
  if(Cypress.env('urlb2c').includes("lentesplus.com/cl")) return
  let nroOrden;

  beforeEach(() => {
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql`, (req) => {
      // Queries
      console.log(req.body)
      aliasQuery(req,'createCart')
      aliasQuery(req,'signIn')
      aliasQuery(req,'mergeCarts')
      aliasQuery(req, 'getProductsBySku')
      aliasQuery(req, 'addSimpleProductToCart')
      aliasQuery(req, 'SetShippingMethod')
      aliasQuery(req, 'resolveWompiOrderStatus')
      aliasQuery(req, 'placeOrder')
      aliasQuery(req, 'setSelectedPaymentMethod')
      aliasQuery(req, 'setBillingAddress')
      aliasQuery(req, 'SetCustomerAddressOnCart')
    })

    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getLocale**`).as('getLocale')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+checkUserIsAuthed**`).as('checkUserIsAuthed')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+cmsBlocks**`).as('cmsBlocks')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getKeys**`).as('userPopup')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getRootCategoryId**`).as('yaTengoCuentaBtn')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+accountChipQuery**`).as('accountChipQuery')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getItemCount**`).as('getItemCount')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getCustomer**`).as('getCustomer')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getCartDetails**`).as('getCartDetails')

    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+productSearch**`).as('productSearch')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getProductFiltersBySearch**`).as('getProductFiltersBySearch')

    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getProductDetailForProductPage**`).as('getProductDetailForProductPage')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getProductRatingInfoQuery**`).as('getProductRatingInfoQuery')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GetBreadcrumbs**`).as('GetBreadcrumbs')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getIsDisableProductoLenQuery**`).as('getIsDisableProductoLenQuery')

    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GetCartDetails**`).as('GetCartDetailsQuery')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getPriceSummary**`).as('getPriceSummary')

    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getProductsBySku**`).as('dataCarrouselCart')

    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getProductListing**`).as('getProductListing')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GetCustomer**`).as('GetCustomer')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+deliveryTimeCart**`).as('deliveryTimeCart')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getSelectedShippingAddress**`).as('getSelectedShippingAddress')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GetShippingInformation**`).as('GetShippingInformation')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GetCustomerAddresses**`).as('GetCustomerAddresses')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GetCustomerCartAddress**`).as('GetCustomerCartAddress')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getItemsInCart**`).as('getItemsInCart')

    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getPaymentInformation**`).as('getPaymentInformation')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=%7BcustomerPaymentTokens**`).as('customerPaymentTokens')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getOrderDetails**`).as('getOrderDetails')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getGeneralRatingInfo**`).as('getGeneralRatingInfo')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getMercadoPagoResult**`).as('getMercadoPagoResult')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getOrderDetailsLen**`).as('getOrderDetailsLen')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getShippingTime**`).as('getShippingTime')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getSelectedAndAvailableShippingMethods**`).as('getSelectedAndAvailableShippingMethods')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getPointCart**`).as('getPointCart')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getStatusPlusUp**`).as('getStatusPlusUp')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GET_LOYALTY_LEVEL**`).as('getLoyaltyLevel')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GET_CONFIG_LENTES_SHIPPING**`).as('getConfigLentesShipping')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getSelectedAndAvailableShippingMethods**`).as('getSelectedAndAvailableShippingMethods')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+cart**`).as('getCart')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getAppliedCoupons**`).as('getAppliedCoupons')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GetCustomerOrders**`).as('getCustomerOrders')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GetCustomerOrdersPaginated**`).as('getCustomerOrdersPaginated')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+getRMAStoreConfig**`).as('getRMAStoreConfig')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GET_CUSTOMER_SUSPENDED_SUBSCRIPTIONS**`).as('getCustomerSuspendedSubscriptions')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GET_CUSTOMER_ACTIVE_SUBSCRIPTIONS**`).as('getCustomerActiveSubscriptions')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GetCustomerInformation**`).as('getCustomerInformation')
    cy.intercept(`${Cypress.env('urlb2c_base')}/graphql?query=query+GetProductThumbnailsByURLKey**`).as('getProductThumbnailsByURLKey')

  })

    //Ingresar a Lentesplus pwa
  it('Test B2C Plusup    ', () => {
    cy.log(user)
    cy.viewport(1280, 900);

    // Pra este test, tener en cuenta que la tienda en Chile no dispone de Pagos en Efectivo
    if(Cypress.env('urlb2c').includes("lentesplus.com/cl")) return

    cy.visit(Cypress.env('urlb2c'));

    // Click user pop up
    cy.wait('@getLocale')
    cy.wait('@checkUserIsAuthed')
    cy.wait('@cmsBlocks')
    cy.get('.headerDesktop-lpi_0002_user-2RI', { timeout: 20000 }).click()
    cy.wait('@userPopup')
    cy.wait(5000)

    // Click YA TENGO CUENTA LENTESPLUS
    cy.get('.login-tengoCuenta-3uc').click({force: true})
    cy.wait('@yaTengoCuentaBtn')
    cy.get('.signIn-form-1sq input[name="email"]').type(user.email, {force: true})
    cy.get('.signIn-form-1sq input[name="password"]', { timeout: 10000 }).type(user.password, { timeout: 30000 })
    cy.get('.signIn-ingresar-3Oc').click()
    cy.wait('@gqlsignInQuery')
    // cy.wait('@accountChipQuery')
    cy.wait('@gqlcreateCartQuery')
    cy.wait('@getItemCount')
    cy.wait('@gqlmergeCartsQuery')
    cy.wait('@getCustomer')
    cy.wait('@getCartDetails')

    cy.get('.successDiv-headerText-2xN > span')
      .should('contain', 'Inicio de sesión exitoso')
    cy.get('.successDiv-infoText-3UQ > :nth-child(1)')
      .should('contain', 'Hola')
    cy.get('.successDiv-infoText-3UQ > :nth-child(2)')
      .should('contain', '¡Qué bueno verte!')

    // Click en el Continuar del Login Modal una vez loueado
    cy.get('.successDiv-continuar-2ev').click()

  })
})
