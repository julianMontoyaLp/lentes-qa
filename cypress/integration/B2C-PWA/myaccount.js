//Ingresar a Lentesplus
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})
import UsuarioAR from '../../fixtures/testspwa/UsuarioAR.json'
import UsuarioCO from '../../fixtures/testspwa/UsuarioCO.json'
import UsuarioCL from '../../fixtures/testspwa/UsuarioCL.json'
import UsuarioMX from '../../fixtures/testspwa/UsuarioMX.json'

describe('B2C - Login My account', () => {
    //Ingresar a Lentesplus pwa

    it('B2C - Login My account ', () => {
        cy.viewport(1280, 900);
        cy.visit(Cypress.env('urlb2c'));
        let Usuario = ''
        if(Cypress.env('urlb2c').includes("lentesplus.com/ar")  ){
            Usuario = UsuarioAR
        }
        if(Cypress.env('urlb2c').includes("lentesplus.com/co") ){
            Usuario = UsuarioCO
        }
        if(Cypress.env('urlb2c').includes("lentesplus.com/cl") ){
            Usuario = UsuarioCL
        }
        if(Cypress.env('urlb2c').includes("lentesplus.com/mx") ){
            Usuario = UsuarioMX
        }


        //boton recomprar y login
        cy.get('.headerDesktop-recomprar-1c2 > button').click();
        cy.get('.login-frameLogin-5p9').should('be.visible')
        cy.get('.login-tengoCuenta-3uc').click();
        cy.get('input[name="email"]').type(Usuario.usuario_email);
        cy.get('input[name="password"]').type(Usuario.usuario_pass);

        //cy.get('#e62aef2d-7344-4e79-b85d-77a2d1079957').type(UsuarioCO.usuario_email);
        //cy.get('.textInput-input-BJ7').type(UsuarioCO.usuario_email);
        //cy.get('#90b7d8da-27ac-4676-9e72-310035507698').type(UsuarioCO.usuario_email);
        //cy.get('#95f47094-f9ee-44a8-ac0b-fabccdf0b5f1').type(UsuarioCO.usuario_pass);
        cy.get('.signIn-ingresar-3Oc').click();
        cy.wait(2000)
        cy.get('[style="margin-top: 10px;"] > :nth-child(2) > :nth-child(1) > :nth-child(1) > :nth-child(2) > span').contains('Inicio de sesión exitoso');
    })


})
