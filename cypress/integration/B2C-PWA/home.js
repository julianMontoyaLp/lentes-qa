//Ingresar a Lentesplus
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

describe('B2C - Login', () => {
    //Ingresar a Lentesplus pwa



    it('Test B2C Home Generales', () => {
        cy.viewport(1280, 900);
        cy.visit(Cypress.env('urlb2c'));


        if(Cypress.env('urlb2c').includes("lentesplus.com/ar")  || Cypress.env('urlb2c').includes("lentesplus.com/co")  || Cypress.env('urlb2c').includes("lentesplus.com/cl") || Cypress.env('urlb2c').includes("lentesplus.com/mx")){
            //Click Banner
            cy.get('.principalSlider-principalSlider-p6v > .slick-slider > .slick-next').click();
            cy.get('.principalSlider-principalSlider-p6v > .slick-slider > .slick-next').click();

            //Click Carrusel de marcas
            cy.get('.brandsSlider-carousel-uDF > .slick-slider > .slick-next').click();
            cy.get('.brandsSlider-carousel-uDF > .slick-slider > .slick-next').click();

            // Click productos de la semana
            cy.get('.homePage-recomendedProducts-24m > .slick-slider > .slick-next').click()
            cy.get('.homePage-recomendedProducts-24m > .slick-slider > .slick-next').click()

           // cy.get('.homeRating-container-3iZ').should('be.visible')
        }else{
            //ELSE DE CASOS B2C NO PWA

                //Click Banner
                cy.get('.owl-carousel-custom-13 > .owl-controls > .owl-nav > .owl-next').click();
                cy.get('.owl-carousel-custom-13 > .owl-controls > .owl-nav > .owl-next').click();

                //Click Carrusel de marcas
                cy.get('.owl-carousel-custom-25 > .owl-controls > .owl-nav > .owl-next').click();
                cy.get('.owl-carousel-custom-25 > .owl-controls > .owl-nav > .owl-next').click();

            // Click productos de la semana
            cy.get('.owl-carousel-products-bestsell_products > .owl-controls > .owl-nav > .owl-next').click()
            cy.get('.owl-carousel-products-bestsell_products > .owl-controls > .owl-nav > .owl-next').click()

            //Validacion Opiniones Verificadas
            cy.get('.home-rating').should('be.visible')

            //Validacion NewsLater
            cy.get('#newsletter').should('be.visible')
            cy.get('#newsletter-validate-detail > .actions > .action').should('be.visible')

            //Validacion footer
            cy.get('.footer-custom-section').should('be.visible')
        }
    })

    it('Test B2C menu superior, vinculos', () => {

        //Validacion menu superior y vinculos por store
        if(Cypress.env('urlb2c').includes("lentesplus.com/ar") ){
            cy.get(':nth-child(1) > .headerCategoryL1-categoryName-neS').should('be.visible').contains('Lentes de Contacto')
            cy.get(':nth-child(2) > .headerCategoryL1-categoryName-neS').should('be.visible').contains('Soluciones y Gotas')
            cy.get(':nth-child(3) > .headerCategoryL1-categoryName-neS').should('be.visible').contains('Anteojos')
            cy.get(':nth-child(4) > .headerCategoryL1-categoryName-neS').should('be.visible').contains('PlusUp')
            cy.get(':nth-child(5) > .headerCategoryL1-categoryName-neS').should('be.visible').contains('Todos los productos')
            cy.get(':nth-child(6) > .headerCategoryL1-categoryName-neS').should('be.visible').contains('Agenda tu cita')
            //Validacion de vinculos
            cy.get('.clients-btnNewClients-3hD > :nth-child(1) > a').should('have.attr', 'href', 'http://guia-usuario-nuevo.lentesplus.com/#/ar/ncl')
            cy.get('.clients-btnNewClients-3hD > :nth-child(2) > a').should('have.attr', 'href', 'http://guia-usuario-nuevo.lentesplus.com/#/ar/nlp')
        }else if(Cypress.env('urlb2c').includes("lentesplus.com/co")){
            cy.get(':nth-child(1) > .headerCategoryL1-categoryName-neS').should('be.visible').contains('Lentes de Contacto')
            cy.get(':nth-child(2) > .headerCategoryL1-categoryName-neS').should('be.visible').contains('Soluciones y Gotas')
            cy.get(':nth-child(3) > .headerCategoryL1-categoryName-neS').should('be.visible').contains('Gafas')
            cy.get(':nth-child(4) > .headerCategoryL1-categoryName-neS').should('be.visible').contains('Cuidado y Belleza')
            cy.get(':nth-child(5) > .headerCategoryL1-categoryName-neS').should('be.visible').contains('PlusUp')
            cy.get(':nth-child(6) > .headerCategoryL1-categoryName-neS').should('be.visible').contains('Todos los productos')
            cy.get(':nth-child(7) > .headerCategoryL1-categoryName-neS').should('be.visible').contains('Agenda tu cita')

            //Validacion de vinculos
            cy.get('.clients-btnNewClients-3hD > :nth-child(1) > a').should('have.attr', 'href', 'http://guia-usuario-nuevo.lentesplus.com/#/co/ncl')
            cy.get('.clients-btnNewClients-3hD > :nth-child(2) > a').should('have.attr', 'href', 'http://guia-usuario-nuevo.lentesplus.com/#/co/nlp')
        }else if(Cypress.env('urlb2c').includes("lentesplus.com/mx")){
            cy.get(':nth-child(1) > .headerCategoryL1-categoryName-neS').should('be.visible').contains('Lentes de Contacto')
            cy.get(':nth-child(2) > .headerCategoryL1-categoryName-neS').should('be.visible').contains('Soluciones')
            cy.get(':nth-child(3) > .headerCategoryL1-categoryName-neS').should('be.visible').contains('Gotas hidratantes')
            cy.get(':nth-child(5) > .headerCategoryL1-categoryName-neS').should('be.visible').contains('PlusUp')
            cy.get(':nth-child(6) > .headerCategoryL1-categoryName-neS').should('be.visible').contains('Todos los productos')

            /* Tradicional
            cy.get('#ui-id-2 > :nth-child(2)').should('be.visible').contains('Lentes de Contacto')
            cy.get('#ui-id-3 > :nth-child(2)').should('be.visible').contains('Soluciones')
            cy.get('#ui-id-4 > :nth-child(2)').should('be.visible').contains('Gotas hidratantes')
            cy.get('#ui-id-5').should('be.visible').contains('PlusUp')
            cy.get('#ui-id-6').should('be.visible').contains('Todos los productos')*/
            //Validacion de vinculos
            //cy.get('.clients-btnNewClients-3hD > :nth-child(1) > a').should('have.attr', 'href', 'http://guia-usuario-nuevo.lentesplus.com/#/co/ncl')
            //cy.get('.clients-btnNewClients-3hD > :nth-child(2) > a').should('have.attr', 'href', 'http://guia-usuario-nuevo.lentesplus.com/#/co/nlp')
        }else if(Cypress.env('urlb2c').includes("lentesplus.com/cl")){
            cy.get(':nth-child(1) > .headerCategoryL1-categoryName-neS').should('be.visible').contains('Lentes de Contacto')
            cy.get(':nth-child(2) > .headerCategoryL1-categoryName-neS').should('be.visible').contains('Soluciones Multipropósito')
            cy.get(':nth-child(3) > .headerCategoryL1-categoryName-neS').should('be.visible').contains('Gotas Lubricantes')
            cy.get(':nth-child(4) > .headerCategoryL1-categoryName-neS').should('be.visible').contains('Anteojos')
            cy.get(':nth-child(5) > .headerCategoryL1-categoryName-neS').should('be.visible').contains('PlusUp')
            cy.get(':nth-child(6) > .headerCategoryL1-categoryName-neS').should('be.visible').contains('Todos los productos')

            /*cy.get('#ui-id-2 > :nth-child(2)').should('be.visible').contains('Lentes de Contacto')
            cy.get('#ui-id-3 > :nth-child(2)').should('be.visible').contains('Soluciones')
            cy.get('#ui-id-4 > :nth-child(2)').should('be.visible').contains('Gotas Lubricantes')
            cy.get('#ui-id-5').should('be.visible').contains('Anteojos')
            cy.get('#ui-id-6').should('be.visible').contains('PlusUp')
            cy.get('#ui-id-7').should('be.visible').contains('Todos los productos')*/
            //Validacion de vinculos
            //cy.get('.clients-btnNewClients-3hD > :nth-child(1) > a').should('have.attr', 'href', 'http://guia-usuario-nuevo.lentesplus.com/#/co/ncl')
            //cy.get('.clients-btnNewClients-3hD > :nth-child(2) > a').should('have.attr', 'href', 'http://guia-usuario-nuevo.lentesplus.com/#/co/nlp')
        }else{
            cy.get('.nav-1 > .level-top').should('be.visible').contains('Store no identificado')
        }
    })

    it('Test B2C LOGIN Modal', () => {
       // cy.viewport(1280, 900);
        //cy.visit(Cypress.env('urlb2c'));
        //Valida el modal de login
        if(Cypress.env('urlb2c').includes("lentesplus.com/ar")  || Cypress.env('urlb2c').includes("lentesplus.com/co")  || Cypress.env('urlb2c').includes("lentesplus.com/cl") || Cypress.env('urlb2c').includes("lentesplus.com/mx") ){
            cy.get('.headerDesktop-recomprar-1c2 > button').click();
            cy.get('.login-frameLogin-5p9').should('be.visible')
            cy.get('.login-facebookDiv-gme').should('be.visible')
            cy.get('.login-googleDiv-1eq').should('be.visible')

            cy.get('.login-enviarClave-XD1').should('be.visible')
            cy.get('.login-enviarClave-XD1').click();
            cy.get('.forgotPassword-backButton-2r9').click();
            cy.get('.signIn-regresar-1I4').click();

            cy.get('.login-tengoCuenta-3uc').should('be.visible');
            cy.get('.login-tengoCuenta-3uc').click();
            cy.get('.signIn-regresar-1I4').click();

            cy.get('.login-crearCuenta-2aH').should('be.visible');
            cy.get('.login-crearCuenta-2aH').click();
            cy.get('.createAccount-registrar-2R6').should('be.visible');

            cy.get('.login-closeIconLogin-2bo').click();
        }else{
            cy.get('.reorder-link > .action').click();
            cy.get('.authentication').should('be.visible')
            cy.get('.facebook-login > .btn').should('be.visible')
            cy.get('.google-login > .btn').should('be.visible')

            cy.get('.block-content > :nth-child(1) > .action').should('be.visible')
            cy.get('.block-content > :nth-child(1) > .action').click();
            cy.get('#social-form-quick-access > .fieldset > .actions-toolbar > div.secondary > .action').click();

            cy.get('.block-content > :nth-child(2) > .action').should('be.visible');
            cy.get('.block-content > :nth-child(2) > .action').click();
            cy.get('#social-form-login > .fieldset > .actions-toolbar > div.secondary > .action').click();

            cy.get('.block-content > .row > .col-12 > .action').should('be.visible');
            cy.get('.block-content > .row > .col-12 > .action').click();
            cy.get('#firstname').should('be.visible');

            cy.get('.mfp-close').click();
        }
    })

})