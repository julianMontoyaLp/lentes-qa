// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add('login', (username, password) => {
    cy.get('#username').type(username)
    cy.get('#login').type(password, {log: false})
    cy.get('.action-login').click()
})

Cypress.Commands.add('searchCustomerByEmailAndWebsite', (email, website) => {
    cy.get('[data-bind="afterRender: $data.setToolbarNode"] > :nth-child(1) > .data-grid-search-control-wrap > #fulltext').clear().type(`{enter}`)
    // cy.wait(3000)
    cy.get('[data-bind="afterRender: $data.setToolbarNode"] > :nth-child(1) > .data-grid-filters-actions-wrap > .data-grid-filters-action-wrap > .action-default').click()
    // cy.wait(2000)
    cy.scrollTo(0,400, {duration:2000})
    cy.get('input[type="text"][name="email"]').eq(0).clear().type(email)
    cy.get('select[name="website_id"]').eq(0).select(website)
    cy.scrollTo(0,600, {duration:2000})
    cy.get('.admin__footer-main-actions > .action-secondary').click()

    // cy.wait(5000)
    cy.get('table.data-grid .data-row')
        .should('exist')

    // cy.wait(5000)
    cy.get('[data-role="grid-wrapper"]')
        .scrollTo('right', {duration: 4000})
    cy.get('.data-grid-actions-cell > .action-menu-item').click()
})

Cypress.Commands.add('fillDataCard', () => {
  if(Cypress.env('urlb2c').includes("lentesplus.com/ar")) {
    cy.get('#cardNumber').type('5011 0542 1120 6753')
  } else if(Cypress.env('urlb2c').includes("lentesplus.com/cl")){
    cy.get('#cardNumber').type('4168 8188 4444 7115')
  } else if(Cypress.env('urlb2c').includes("lentesplus.com/mx")){
    cy.get('#cardNumber').type('4075 5957 1648 3764')
  } else if(Cypress.env('urlb2c').includes("lentesplus.com/co")){
    cy.get('#cardNumber').type('4242 4242 4242 4242')
  }

  cy.get('#cardExpirationMonth').select('11')
  cy.get('#cardExpirationYear').select('2025')
  cy.get('#securityCode').type('123')
  
  if(Cypress.env('urlb2c').includes("lentesplus.com/co")){
    cy.get('#cardholderName').type('APPROVED')
  } else {
    cy.get('#cardholderName').type('APRO')
  }

  if(Cypress.env('urlb2c').includes("lentesplus.com/ar")) {
    cy.get('#docNumber').type('60606060')
  } else if(Cypress.env('urlb2c').includes("lentesplus.com/cl")){
    cy.get('#docNumber').type('11051850-1')
  }
  cy.get('#installments').select('1')
})